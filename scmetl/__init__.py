# -*- coding: utf-8 -*-

"""Top-level package for scmetl."""

__author__ = 'Stephen Dictor'
__email__ = 'sdictor@renew-ap.com'
__version__ = '0.1.0'


# Load configuration
import os, json
from . import loggerinitializer, reportdb, utils, sqla, scmetl
from .reportdb import ReportDatabase

config_var = os.environ.get('SCMETL_CONFIG', None)
config_json = os.environ.get('SCMETL_CONFIG_JSON', None)
config_file = os.environ.get('SCMETL_CONFIG_FILE', None)
if(config_var != None):
    config = config_var
elif(config_json != None):
    config = json.loads(config_json)
elif(config_file != None):
    with open(config_file,'r') as f:
        config = json.load(f)
else:
    raise LookupError("It is necessary to set the SCMETL_CONFIG_JSON or SCMETL_CONFIG_FILE environmental variable before running this script.")

#Initialize logger
loggerinitializer.initialize_logger(config['logs_dir'])

#
# Helper functions
#

import json
import pandas as pd
import requests
import pandas as pd
from datetime import datetime
import time
import os
import requests
from requests import HTTPError
from pathlib import Path
import sys
import logging

from .. import utils as dutils
from .. import reportdb

#%%
def helper_json_cols(df):
    # Scrub the df: convert object column types to string
    json_col_types = dutils.find_listtype_cols(df,700)
    if(len(json_col_types)>0):
        df[json_col_types] = df[json_col_types].where((pd.notnull(df[json_col_types])), '')
        df[json_col_types] = df[json_col_types].astype(str)
        dutils.rewrite_json_cols(df,json_col_types)
    return(df)

def helper_scrub_df(df):
    # Scrub the df
    dutils.cleanDFColnames(df)
    dutils.make_colnames_unique(df)
    df = df.infer_objects()
    dutils.pd_convert_all_dates(df)
    df = dutils.sort_df_cols(df)
    return(df)

def suggest_ts_filename(prefix = 'untitled', page_num = 0, ts_datetime = datetime.now()):
    """ Suggests filename with timestamp """
    file_prefix = prefix + "_p" + str(page_num) if (page_num != 0) else prefix
    fname = file_prefix + "_" +  ts_datetime.strftime("%Y%m%dT%H%M%S") + ".json"
    return(fname)

def suggest_ts_filedir(base_dir = Path.cwd(), prefix = 'untitled', subdir = '', ts_datetime = datetime.now()):
    """ Suggests filedir with timestamp """
    filedir = Path(base_dir).joinpath(prefix)
    if subdir == '':
        filedir = filedir.joinpath(ts_datetime.strftime('%Y%m%d'))
    else:
        filedir = filedir.joinpath(ts_datetime.strftime('%Y%m%d'),subdir)
    return(filedir)

def suggest_ts_filepath(base_dir = Path.cwd(), prefix = 'untitled', subdir = '', page_num = 0, ts_datetime = datetime.now()):
    """ Suggests file path with timestamp """
    #base_dir = pathlib.Path(base_dir).joinpath(prefix)
    filedir = suggest_ts_filedir(base_dir, prefix, subdir, ts_datetime=ts_datetime)
    fname = suggest_ts_filename(prefix, page_num, ts_datetime=datetime.now())
    
    #file_path.joinpath(prefix).mkdir(parents=True, exist_ok=True)    
    #out_folder.mkdir(parents=True, exist_ok=True)
    #print ("out_folder= " + str(out_folder))    
        
    return(filedir.joinpath(fname))

def save_timestamped_file(content, base_dir, file_prefix = '', subdir = '', page_num = 0):
    #file_path.joinpath(prefix).mkdir(parents=True, exist_ok=True)
    #if subdir == '':
    #    out_folder = base_dir.joinpath(datetime.now().strftime('%Y%m%d'))
    #else:
    #    out_folder = base_dir.joinpath(datetime.now().strftime('%Y%m%d'),subdir)
    out_folder = suggest_ts_filedir(base_dir, file_prefix, subdir,ts_datetime = datetime.now())
    out_folder.mkdir(parents=True, exist_ok=True)
    
    #print ("out_folder= " + str(out_folder))
    
    #fname = file_prefix + "_" +  datetime.now().strftime("%Y%m%dT%H%M%S") + ".json"
    #print ("fname= " + fname)
    out_file = suggest_ts_filepath(base_dir, file_prefix, subdir, page_num, ts_datetime = datetime.now())
    
    #with open(out_folder.joinpath(fname), "w") as f:
    with open(out_file, "w") as f:
        f.write(content)
        #json.dump(content,f)
        #j_content = json.loads(content)
        
    #return(out_folder.joinpath(fname))
    return(out_file)

def save_page_to_file(content, file_path, prefix, page_num):
    return(save_timestamped_file(
        content = content, 
        base_dir = file_path,
        file_prefix = prefix,
        subdir = '',
        page_num = page_num
))
#    return(save_timestamped_file(
#        content = content, 
#        base_dir = file_path.joinpath(prefix),
#        file_prefix = prefix + "_p" + str(page_num)
#))


completed_steps = []
skipped_steps = []
uncompleted_steps = []
comments = []

def parse_progress_data( progress_dict ):
    """ Parse Kissflow Progress data & format for load to database """

    # Recursive function to extract list of steps & ignore the branches
    global completed_steps, skipped_steps, uncompleted_steps, comments
    completed_steps = []
    skipped_steps = []
    uncompleted_steps = []
    comments = []

    def walkDict( aDict, visitor, path=(), prefix="" ):
        global completed_steps, skipped_steps, uncompleted_steps, comments
        #print(str(path) + " | " + prefix)
        if (('Name' in aDict.keys()) & ('Type' in aDict.keys()) & ('Status' in aDict.keys())):
            # This is a Step object
            aDict['Name'] = prefix + aDict['Name']
            if(aDict['Status'] == 'Skipped'):
                skipped_steps.append(aDict)
            elif('Completed At' in aDict.keys()):
                completed_steps.append(aDict)
            else:
                uncompleted_steps.append(aDict)

            # See if there are comments
            if('Comments' in aDict.keys()):
                for c in aDict['Comments']:
                    com = {
                        "comment": c["Comment"],
                        "comment_user": c["Commented By"],
                        "comment_time": c["Commented At"],
                        "comment_step":aDict['Name']
                    }
                    comments.append(com)
        else:
            # Use Branch name as prefix
            if('Branch Name' in aDict.keys()):
                pref = aDict['Branch Name'] + " - "
            else:
                pref = ""

            for k, v in aDict.items():
                if (type(v) == list):
                    # This is an array (probably Steps)
                    for l in v:
                        walkDict(l, visitor, path+(k,), pref)
                elif type(v) != dict:
                    #visitor( path, v )
                    pass
                else:
                    walkDict(v, visitor, path+(k,))

    def callback_func( path, element ):
        print(path, element)

    walkDict( progress_dict, callback_func )
    
    # Build a data frame    
    comp = [dict(description= a['Name'] + (' ' + a['Status'] if (a['Status'] != 'Completed') else ''), step=a['Name'], action=a['Status'], start= a['Assigned At'], end = a['Completed At'], user=a['Completed By']) for a in completed_steps]
    uncomp = [a['Name'] for a in uncompleted_steps if a['Name'] != 'End']
    skip = [a['Name'] for a in skipped_steps if a['Name'] != 'End']
    if('Status' in progress_dict.keys()):
        status = progress_dict['Status']
    elif( ('Completed Percentage' in progress_dict.keys()) and (progress_dict['Completed Percentage'] == 100) ):
        status = "Complete"
    else:
        status = ""
    if('Subject' in progress_dict.keys()):
        df = pd.DataFrame({
            "app" : "",
            "endpoint" : "",
            "item_id" : "",
            "subject" : [progress_dict['Subject']],
            "status" : [status],
            "percent_complete" : [progress_dict['Completed Percentage']],
            "completed_steps" : json.dumps(comp),
            "skipped_steps" : json.dumps(skip),
            "notstarted_steps" : json.dumps(uncomp),
            "comments" : json.dumps(comments),
            "date_loaded" : [datetime.utcnow().date()],
            "datetime_loaded" : [datetime.utcnow()]
            
        })
    else:
        logging.warning("Progress data couldn't be parsed:")
        logging.warning(json.dumps(progress_dict))
        df = pd.DataFrame({
            "app" : "",
            "endpoint" : "",
            "item_id" : "",
            "subject" : "",
            "status" : "",
            "percent_complete" : None,
            "completed_steps" : "",
            "skipped_steps" : "",
            "notstarted_steps" : "",
            "comments" : "",
            "date_loaded" : [datetime.utcnow().date()],
            "datetime_loaded" : [datetime.utcnow()]
            
        })
       
    return(df)
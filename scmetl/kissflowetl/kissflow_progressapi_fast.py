import logging
#import sys, os
import json, requests, pandas as pd, urllib, re
from datetime import datetime
from dateutil import parser
import time, sqlalchemy
from requests import HTTPError
from pathlib import Path

from . import kissflow_helpers as helpers

from sqlalchemy import Table, Column, Integer, Float, String, MetaData, Date, DateTime, DECIMAL, Text, LargeBinary, text
#from collections import OrderedDict

#from requests_threads import AsyncSession 
import asyncio
import concurrent.futures
import requests

def request_retry(url, headers, tries = 4):
    count_tries = 0
    is_success = False
    
    while(count_tries <= tries & is_success == False):
        count_tries += 1
        print("Try number " + str(count_tries))
        response = requests.get(url, headers= headers)
        is_success = ( (response.status_code == 200) and not('error' in json.loads(response.text).keys()) ) # and not('error' in json.loads(response.text).keys())
        if(not(is_success)):
            time.sleep(5)
    return(response)

def kflow_read_app_progressapi_fast(batch_jobs, app_name, pages = range(0), save_to_file = True, skip_completed = True):
    # URL encode the endpoint name
    endpoint_name = urllib.parse.quote(batch_jobs.source_apps[app_name]["process_name"], safe='')

    def make_table():
        # Create table in database
        metadata = MetaData()
        new_table = Table(
                "kissflow_" + app_name + "_progress_updates", metadata,
                Column('pkid', Integer, primary_key=True, autoincrement=True),
                Column('app', Text),
                Column('endpoint', Text),
                Column('item_id', Text),
                Column('subject', Text),
                Column('status', Text),
                Column('percent_complete', Float),
                Column('completed_steps', Text),
                Column('skipped_steps', Text),
                Column('notstarted_steps', Text),
                Column('comments', Text),
                Column('date_loaded', Date),
                Column('datetime_loaded', DateTime)
            )

        new_table.drop(batch_jobs.sqlite.ENGINE, checkfirst=True) # Drop if exists
        metadata.create_all(batch_jobs.sqlite.ENGINE) # Create Table

    make_table()

    def get_headers():
        q = "select id from kissflow_"+app_name+"_headers"
        if(skip_completed):
            # Only get progress data for the records that are not already 'complete'. HOWEVER, if the last action performed as less than 6 weeks ago, get the data anyways
            q = q + " where lower(process_step) <> 'complete'"
        
        # Read header records from database
        with batch_jobs.sqlite.ENGINE.begin() as con:
            df_all = pd.read_sql_query(text(q),con)
        
        return(df_all)
    
    df_all = get_headers()

    # By default, get data for all records
    if(pages == range(0)):
        pages = range(0,len(df_all))
                    
    df_subset = df_all[min(pages):max(pages)]
            
    def get_url_and_headers(id):
        url = "https://kf-0000980.appspot.com/api/1/{end}/{id}/progress".format(end=endpoint_name, id=id)
        headers= {'api_key': batch_jobs.KISSFLOW_SECRET['key'], 'email_id':batch_jobs.KISSFLOW_SECRET['user']}
        return url, headers

    # Helper to write a result to the database
    def write_to_db(row):
        try:
            df = helpers.parse_progress_data(json.loads(row['response'].text))
            if(df['subject'] != ""):
                df['app'] = app_name
                df['endpoint'] = endpoint_name
                df['item_id'] = row['id']
                df['date_loaded'] = datetime.utcnow().date()
                df['datetime_loaded'] = datetime.utcnow()
                batch_jobs.sqlite.importDF(df, "kissflow_" + app_name + "_progress_updates", if_exists="append")
            else:
                logging.warning("Skipped database insert")
        except Exception as e:
            logging.warning("Error occurred while parsing & inserting data:")
            logging.warning(json.dumps({row.status_code, row.text}))
    # Hit the progress data API for each record
    # session = AsyncSession(n=len(df_subset))
    # async def _run_progress_requests():
    #     rs = []
    #     for i, row in df_subset.iterrows():
    #         url, headers = get_url_and_headers(row['id'])
    #         rs.append({"id":row['id'], "response": await session.get(url, headers) })
    #     return(rs)

    # rs = session.run(_run_progress_requests)

    # See https://skipperkongen.dk/2016/09/09/easy-parallel-http-requests-with-python-and-asyncio/
    def get_result(id, url, headers):
        #logging.warning(json.dumps({"id":id, "url": url, "headers": headers}))
        res = requests.get(url, headers= headers)
        #logging.warning(json.dumps({"status_code":res.status_code, "text": res.text}))
        
        # Save to database
        if(res.status_code == 200):
            result = {"id":id, "response": res }
            write_to_db(result)
            if(save_to_file):
                #helpers.save_page_to_file(res.text, file_path=batch_jobs.KISSFLOW_DIR, prefix=app_name+"_progress_updates", page_num=i)
                helpers.save_page_to_file(res.text, file_path=batch_jobs.KISSFLOW_DIR, prefix=app_name+"_progress_updates_" + id + "_")
        else:
            logging.warning("Retrying progress data download for record with id " + id )
            url, headers = get_url_and_headers(id)
            res = request_retry(url, headers)
            result = {"id":id, "response": res }
            write_to_db(result)
            if(save_to_file):
                #helpers.save_page_to_file(res.text, file_path=batch_jobs.KISSFLOW_DIR, prefix=app_name+"_progress_updates", page_num=i)
                helpers.save_page_to_file(res.text, file_path=batch_jobs.KISSFLOW_DIR, prefix=app_name+"_progress_updates_" + id + "_")

        return result

    rs = []
    async def _run_progress_requests():
        with concurrent.futures.ThreadPoolExecutor(max_workers=200) as executor:
            loop = asyncio.get_event_loop()
            #url, headers = get_url_and_headers(row['id'])
            futures = [
                loop.run_in_executor(
                    executor, 
                    get_result, 
                    row['id'],
                    *get_url_and_headers(row['id'])
                )
                for i, row in df_subset.iterrows()
            ]
            for response in await asyncio.gather(*futures):
                rs.append(response)
                pass

    loop = asyncio.get_event_loop()
    loop.run_until_complete(_run_progress_requests())

    # Loop over results
 
    # data_pages = list()
    # for i, row in enumerate(rs):
    #     # Failure - retry
    #     if(row['response'].status_code != 200):
    #         logging.warning("Retrying progress data download for record with id " + row['id'] )
    #         url, headers = get_url_and_headers(row['id'])
    #         row['response'] = request_retry(url, headers)
    #         # Save to database
    #         write_to_db(row)

    #     # Success
    #     if(save_to_file):
    #         helpers.save_page_to_file(row['response'].text, file_path=batch_jobs.KISSFLOW_DIR, prefix=app_name+"_progress_updates", page_num=i)

        # Save to database
        #write_to_db(row)
import logging as log
#log = logging.getLogger()
#log.setLevel(logging.DEBUG)
from .. import loggerinitializer

#loggerinitializer.initialize_logger('/home/steve12d/logs')
#loggerinitializer.initialize_logger('C:\\users\dicto\logs')

import sys, os
import json, requests, pandas as pd, urllib, re
from datetime import datetime
from dateutil import parser
import time, sqlalchemy
from requests import HTTPError
from pathlib import Path

from .. import utils as dutils
from .. import reportdb

from . import kissflow_helpers as helpers

from sqlalchemy import Table, Column, Integer, Float, String, MetaData, Date, DateTime, DECIMAL, Text, LargeBinary, text
from collections import OrderedDict
import logging
from .kissflow_progressapi_fast import kflow_read_app_progressapi_fast

class KissflowBatchJobs:

    def __init__(self, user_parameters):
        ''' Constructor '''

        # Load User Parameters (Dict)
        self.user_parameters = user_parameters

        # Config json content or file
        config_var = os.environ.get('SCMETL_CONFIG', None)
        config_json = os.environ.get('SCMETL_CONFIG_JSON', None)
        config_file = os.environ.get('SCMETL_CONFIG_FILE', None)
        if(config_var != None):
            self.config = config_var
        elif(config_json != None):
            self.config = json.loads(config_json)
        elif(config_file != None):
            with open(config_file,'r') as f:
                self.config = json.load(f)
        else:
            raise LookupError("It is necessary to set the SCMETL_CONFIG_JSON or SCMETL_CONFIG_FILE environmental variable before running this script.")

        ''' Apply template to 1) file paths & 2) database connection strings by formatting string values in the inner dictionary
            This makes it possible to update database names that have a yyyymmdd timestamp.
        '''
        def format_nested_dict(nd):
            results = nd
            for k, level2 in nd.items():
                for k2, v2 in level2.items():
                    if(isinstance(v2,str)):
                        results[k][k2] = v2.format(yyyymmdd=self.user_parameters['yyyymmdd'])
            return(results)
        
        self.config['db_conns'] = format_nested_dict(self.config['db_conns'])
        self.db_conns = self.config['db_conns']
        #self.config['kissflow_apps'] = format_nested_dict(self.config['kissflow_apps'])

        # Files for import
        #self.MAX_NUM_DOWNLOAD_PAGES = 1
        self.source_apps = self.config['kissflow_apps']

        self.KISSFLOW_DIR = Path(self.config['kissflow_dir'])

        #self.SCM_TRUTH = self.db_conns['scm_truth']
        self.SCM_TRUTH = self.db_conns['scm_truth']
        self.SCM_SQLITE = self.db_conns['sqlite_local_dev']
        self.KISSFLOW_SECRET = self.config['kissflow_secret']
        #self.SCM_ARCHIVE = {"name_or_url":"mysql+pymysql://scm_truth:juliavargas632@greenfuturo.com:3306/scm_archive"}

        self.sqlite = reportdb.ReportDatabase(DB_CONN_STRING = self.SCM_SQLITE)
        self.scm = reportdb.ReportDatabase(DB_CONN_STRING = self.SCM_TRUTH)
        #self.scm.setDBConnString(self.SCM_TRUTH)

        #self.scm.setKissflowSecret(self.config['kissflow_secret'])
        
        # List of the transforms that are available (must be accurate for the run_transforms and transform functions to work properly)
        self.transforms=OrderedDict([
            ('prv_kpi_calcs', kissflow_transforms.prv_kpi_calcs)
        ])

    ## ***************************************************************************************************************
    ## 2. DOWNLOAD & SAVE RAW DATA -------------------------------------------------------------------------------
    ## ***************************************************************************************************************

    def transform(self, func_name, **kwargs):
        """ Function to dispatch a one of the transform functions on the batchjobs object """        
        # Do the calculations
        (self.transforms[func_name])(batchjobs=self, **kwargs)
        #except KeyError:
        #    raise ValueError('The function ' + func_name + ' does not exist')

    def kflow_json(self, endpoint_name):
        # See https://realpython.com/python-requests/
        url = 'https://kf-0000980.appspot.com/api/1/' + endpoint_name
        # Only include email id if it has been provided to the function
        try:
            if(self.KISSFLOW_SECRET['user'] == ''):
                response = requests.get(url, headers= {'api_key': self.KISSFLOW_SECRET['key']})
            else:
                response = requests.get(url, headers= {'api_key': self.KISSFLOW_SECRET['key'], 'email_id':self.KISSFLOW_SECRET['user']})
            # If the response was successful, no Exception will be raised
            response.raise_for_status()
        except requests.exceptions.HTTPError as http_err:
            success = False
            message = f'HTTP error occurred: {http_err}'
            content = response.text
        except Exception as err:
            success = False
            message = f'Other error occurred: {err}'
            content = response.text
        else:
            success = True
            message = 'Success'
            content = response.text
        return({'success':success, 'message':message ,'content':content })

    def kflow_json_tries(self, endpoint_name, tries = 4):
        count_tries = 0
        is_success = False
        data_download = {'success':False, 'message':'No tries attempted' ,'content':'' }
        while(count_tries <= tries & is_success == False):
            count_tries += 1
            #print("Try number " + str(count_tries))
            data_download = self.kflow_json(endpoint_name)
            is_success = data_download['success']
            if(not(is_success)):
                time.sleep(5)
        return(data_download)

    def download_one_table_from_remote(self, source_table, rename_to = ""):
        """ Download a table from MySql database & import into SQLite database """
        if(rename_to ==""):
            rename_to = source_table
        
        q = "select * from " + source_table

        with self.scm.ENGINE.begin() as cm:
            try:
                df_for_import = pd.read_sql(text(q), cm)
            except Exception as err:
                df_for_import = pd.read_sql(text(q), cm)
                pass
        self.sqlite.importDF(df_for_import, rename_to, if_exists="replace")

    def download_lookup_tables_from_remote(self):
        inspector = sqlalchemy.inspect(self.scm.ENGINE)
        df = pd.DataFrame(inspector.get_table_names())
        tbl_list = df[df[0].str.contains('lookup')].iloc[:, 0].tolist()

        for table_name in tbl_list:
            with self.scm.ENGINE.begin() as cm:
                try:
                    df_for_import = pd.read_sql("select * from " + table_name, cm)
                except Exception as err:
                    df_for_import = pd.read_sql("select * from " + table_name, cm)
                    pass
            self.sqlite.importDF(df_for_import, table_name, if_exists="replace")

    def kflow_read_app_progress_from_api(self, app_name, pages = range(0), save_to_file = False, skip_completed = True):
        # URL encode the endpoint name
        endpoint_name = urllib.parse.quote(self.source_apps[app_name]["process_name"], safe='')

        # Create table in database
        metadata = MetaData()
        new_table = Table(
                "kissflow_" + app_name + "_progress_updates", metadata,
                Column('pkid', Integer, primary_key=True, autoincrement=True),
                Column('app', Text),
                Column('endpoint', Text),
                Column('item_id', Text),
                Column('subject', Text),
                Column('status', Text),
                Column('percent_complete', Float),
                Column('completed_steps', Text),
                Column('skipped_steps', Text),
                Column('notstarted_steps', Text),
                Column('comments', Text),
                Column('date_loaded', Date),
                Column('datetime_loaded', DateTime)
            )

        new_table.drop(self.sqlite.ENGINE, checkfirst=True) # Drop if exists
        metadata.create_all(self.sqlite.ENGINE) # Create Table

        q = "select id from kissflow_"+app_name+"_headers"
        if(skip_completed):
            # Only get progress data for the records that are not already 'complete'. HOWEVER, if the last action performed is less than 6 weeks ago, get the data anyways
            q = q + " where lower(process_step) <> 'complete'"
        
        # Read header records from database
        with self.sqlite.ENGINE.begin() as con:
            df_all = pd.read_sql_query(text(q),con)

        # By default, get data for all records
        if(pages == range(0)):
            pages = range(0,len(df_all))
                       
        df_subset = df_all[min(pages):max(pages)]
                
        # Hit the progress data API for each record
        data_pages = list()
        for i, row in df_subset.iterrows():
            # Fetch data
            url = "{end}/{id}/progress".format(end=endpoint_name, id=row['id'])
            #logging.info("api endpoint is /" + url)
            data_download = self.kflow_json_tries(url)

            if(data_download['success']):
                # Save to file
                if(save_to_file):
                    helpers.save_page_to_file(data_download['content'], file_path=self.KISSFLOW_DIR, prefix=app_name+"_progress_updates", page_num=i)

                # Save to database
                df = helpers.parse_progress_data(json.loads(data_download['content']))
                df['app'] = app_name
                df['endpoint'] = endpoint_name
                df['item_id'] = row['id']
                df['date_loaded'] = datetime.utcnow().date()
                df['datetime_loaded'] = datetime.utcnow()
                self.sqlite.importDF(df, "kissflow_" + app_name + "_progress_updates", if_exists="append")
            else:
                logging.warning("Failed to load progress data for record with id " + row['id'] + ". " + str(data_download['message']))

    def kflow_progress_upserts(self, app_name, save_local = True, save_remote = True):
        """ Merge the new/ just-downloaded progress data with the prior data, identifying which records require no action, update, or insert """
        if(self.scm.table_exists("kissflow_{}_progress".format(app_name))):
            with self.scm.ENGINE.begin() as cm:
                prev_records = pd.read_sql("select * from kissflow_{}_progress".format(app_name), cm)
                #bj.download_one_table_from_remote("kissflow_prv_progress")
        else:
            prev_records = pd.DataFrame() 

        with self.sqlite.ENGINE.begin() as cm:
            new_records = pd.read_sql("select * from kissflow_{}_progress_updates".format(app_name), cm)

        print("Previously there were {} records, and now there are {} records for updating".format(len(prev_records),len(new_records)))

        # Merge prev_records & new_records
        merged_records = list()
        update_pkids = list() #pkids of the 'update'(not insert) items
        for i, prev_record in enumerate(prev_records.to_dict('records')):
            prev_record
            new_record = new_records.query("item_id == '" + prev_record['item_id']+"'").to_dict('records')
            num_new_records = len(new_record)
            if(num_new_records == 0):
                merged_records.append(prev_record) # Use the previous record, no updates available
            elif(num_new_records >= 1):
                new_record = new_record[0]
                merged_records.append(new_record) # An update is available, use it
                update_pkids.append(str(new_record['pkid']))
                if num_new_records > 1:
                    print("Found more than one progress record for pkid {} [item id {} ] & updated using the first one only.".format(str(new_record['pkid']), prev_record['item_id']))
                #else:
                #    print("Update processed for pkid {} [item id {} ] ".format(str(new_record['pkid']), prev_record['item_id']))            
                    

        # Identify the new records that are to be inserted
        insert_records = new_records.query("pkid not in (" + ",".join(update_pkids) + ")").to_dict('records')
        # Fully merged dataset with updates
        combined = pd.DataFrame.from_records(merged_records + insert_records)
        # Save to SQLite
        if(save_local):
            self.sqlite.importDF(combined, "kissflow_{}_progress".format(app_name), if_exists = 'replace', create_primary_key = True)
        # Save to MySql
        if(save_remote):
            self.scm.importDF(combined, "kissflow_{}_progress".format(app_name), if_exists = 'replace', create_primary_key = True)

    def kflow_read_appdata_from_api(self, app_name, pages, page_size = 50, save_to_file = True):
        #endpoint_name, file_path, file_prefix,
        # URL encode the endpoint name
        # This is needed for apps that have special characters in their names, such as slashes
        endpoint_name = urllib.parse.quote(self.source_apps[app_name]["process_name"], safe='')
        #file_path
        #file_prefix = app_name

        # Create table in database
        metadata = MetaData()
        new_table = Table(
                "kflow_" + app_name, metadata,
                Column('app', Text),
                Column('endpoint', Text),
                Column('page', Text),
                Column('page_size', Text),
                Column('content', LargeBinary),
                Column('date_loaded', Date),
                Column('datetime_loaded', DateTime)
            )
        new_table.drop(self.sqlite.ENGINE, checkfirst=True) # Drop if exists
        metadata.create_all(self.sqlite.ENGINE) # Create Table

        data_pages = list()
        for i in pages:
            # Fetch data
            data_download = self.kflow_json_tries(endpoint_name + '/list/p' + str(i) + '/' + str(page_size))

            # Check contents, make sure there are still more pages
            check_contents = (str(data_download['content']) != '[]') & (str(data_download['content']) != '')

            if data_download['success'] & check_contents:
                # Save to file
                if(save_to_file):
                    helpers.save_page_to_file(data_download['content'], file_path=self.KISSFLOW_DIR, prefix=app_name, page_num=i)

                # Save to database
                df = pd.DataFrame({
                    "app":[app_name],
                    "endpoint":[endpoint_name],
                    "page":[str(i)],
                    "page_size":[str(page_size)],
                    "content":[data_download['content']],
                    "date_loaded": [datetime.utcnow().date()],
                    "datetime_loaded": [datetime.utcnow()]
                })
                #,"datetime_loaded": [datetime.utcnow()],
                self.sqlite.importDF(df, "kflow_" + app_name, if_exists="append")

                # Append to one large object
                #data_pages.append(json.loads(data_download['content']))
            else:
                break
        # Export one large .JSON file
        #return(helpers.save_timestamped_file( json.dumps(data_pages) ,file_path.joinpath(file_prefix), file_prefix ,"combined" ))

    def kflow_read_appdata_from_sqlite(self, app_name):
        """
        Reads the SQLite data table with raw JSON contents of the data pages obtained through Kissflow API
        Returns a dataframe
        """
        data_pages = list()
        
        with self.sqlite.ENGINE.begin() as con:
            original_data = pd.read_sql_query("select * from kflow_"+app_name,con)

        #original_data.head()    
        for row in original_data.to_dict(orient='records'):
            # Fetch data
            data_page = row['content']
            
            # Add page to list
            if((data_page != '') & (data_page != '[]')):
                this_page = json.loads(data_page)
                for item in this_page:
                    data_pages.append(item)

        df = pd.io.json.json_normalize(data_pages)
        return(df)
    
    def kflow_unnest_progress_steps(self, app_name):
        """
        Reads the SQLite data table with progress data &
        unnests the completed steps into their own table
        """
        # UNDER CONSTRUCTION
        data_pages = list()
        
        with self.sqlite.ENGINE.begin() as con:
            original_data = pd.read_sql_query("select * from kissflow_"+app_name+"_progress",con)
            #original_data.head()    
            for row in original_data.to_dict(orient='records'):
                # Fetch data
                data_page = row['completed_steps']

                # Add page to list
                if((data_page != '') & (data_page != '[]')):
                    this_page = json.loads(data_page)
                    # Copy the header data to each row

                    for x in this_page:
                        # Add prefix to keys (colnames)
                        x = {'step_'+k: v for k, v in x.items()}
                        # Copy header data
                        for k, v in row.items():
                            if (k not in ['completed_steps','skipped_steps','notstarted_steps','comments']):
                                x[k] = v
                        # Calculate step duration
                        end = parser.parse(x['step_end'])
                        start = parser.parse(x['step_start'])
                        x['step_days'] = (end-start).total_seconds() / 60 / 60 /24
                        # Append to results
                        data_pages.append(x)

            df = pd.io.json.json_normalize(data_pages)
            
            # Write to database
            self.sqlite.importDF(df, "kissflow_" + app_name + "_progress_steps", if_exists="replace")
            
    def kflow_convert_app_header_data(self, app_name):
        """
        Reads the SQLite data table with raw JSON contents of the data pages obtained through Kissflow API
        Writes a new table to the database with the data normalized into tabular format
        """
        date_loaded = datetime.strptime(self.user_parameters['yyyymmdd'],'%Y%m%d')
        df = self.kflow_read_appdata_from_sqlite(app_name)
        
        # Scrub the data frame & write out the cleaned data
        if(df.empty):
            log.warning("Nothing to load, combined data is empty for kissflow app: " + app_name)
        else:
            df = helpers.helper_scrub_df(df)
            df = helpers.helper_json_cols(df)
            
            # Write to file
            out_folder = helpers.suggest_ts_filedir(self.KISSFLOW_DIR, app_name, 'combined',ts_datetime = date_loaded)
            out_folder.mkdir(parents=True, exist_ok=True)
            
            flat_file = helpers.suggest_ts_filepath(base_dir = self.KISSFLOW_DIR, prefix = app_name, subdir = 'combined',ts_datetime = date_loaded)
            df.to_json(str(flat_file).replace(".json", "_clean.json"))
            df.to_csv(str(flat_file).replace(".json", "_clean.csv"))
            
            # Write to database
            self.sqlite.importDF(df, "kissflow_" + app_name + "_headers", if_exists="replace")

    def kflow_each_download_load_to_database(self, do_execute, app_name, short_name, flat_file):
        # Open the combined file containing all records
        log.debug("Reading for load to DB: " + str(flat_file))
        #with open(flat_file, "r", errors="backslashreplace") as f:
        with open(flat_file, "r", encoding="latin1", errors="backslashreplace") as f:
            jobj = json.load(f)
            flat_list = dutils.flatten_list(jobj)
            df = pd.io.json.json_normalize(flat_list)

        # Scrub the data frame & write cleaned data to file
        if(df.empty):
            log.warning("Nothing to load, combined data is empty for kissflow app: " + app_name)
        else:
            df = helpers.helper_scrub_df(df)
            df = helpers.helper_json_cols(df)
            df.to_json(str(flat_file).replace(".json", "_clean.json"))
            df.to_csv(str(flat_file).replace(".json", "_clean.csv"))
            #df.to_feather(str(flat_file).replace(".json", "_clean.feather"))

        # Import to database
        self.sqlite.importDF(df, "kissflow_" + short_name + "_headers", if_exists="replace")

    # Add out_file attribute to list of app download jobs
    # APPS_FOR_DOWNLOAD['out_file'] = "n.a."

    def read_one_app_api_to_sqlite(self, app_name):
        """
            Reads one app's entire data from Kissflow API & saves a raw table as well as a curated headers table in SQLite database
        """
        
        t_start = time.perf_counter()

        # First, read from API into raw tables
        log.info("Getting API data for " + app_name)
        self.kflow_read_appdata_from_api(app_name, pages=range(1, 200), page_size = 50)
        
        t_end1 = time.perf_counter()

        log.warn('Time to download data for {} : {:0.3f}. Current time is {}'.format(app_name, t_end1 - t_start, time.ctime()))

        # Next, convert from raw tables into curated headers tables
        log.info("Converting data to tabular format (" + app_name + ")")
        self.kflow_convert_app_header_data(app_name)

        t_end2 = time.perf_counter()
        
        log.warn('Time to process data for {} : {:0.3f}. Current time is {}'.format(app_name, t_end2 - t_end1, time.ctime()))
    
    
    def run_one_download(app_name):
        sel_app = self.source_apps[app_name]
        log.debug("Run download? " + str(sel_app["do_execute"]) + ", " + sel_app["short_name"])

        out_file = "n.a."

        # Download header data to file
        if sel_app['do_execute']:
            out_file = self.kflow_download_pages_tofile(
                endpoint_name=sel_app['app_name'],
                file_path=self.KISSFLOW_DIR,
                file_prefix=sel_app['short_name'],
                pages=range(1, 200)
            )
            log.debug("filename=" + str(out_file))

        # Read the file and load to database
        if (sel_app['do_execute'] & (out_file != "n.a.")):
            self.kflow_each_download_load_to_database(sel_app['do_execute'], sel_app['app_name'], sel_app['short_name'], out_file)

        self.source_apps[app_index]["out_file"] = out_file
        return(out_file)

    def run_header_downloads(self):
        #for r in range(0,len(self.source_apps)):
        for key, value in self.source_apps.items():
            #self.run_one_download(self.source_apps[key])
            if(value['do_execute']):
                #self.run_one_download(key)
                self.read_one_app_api_to_sqlite(key)
            else:
                log.warn("Skipping [" + key + "] since 'do_execute' is set to false.")
        return(True)

    def run_progress_downloads(self):
        for key, value in self.source_apps.items():
            if(value['do_execute']):
                self.kflow_read_app_progress_from_api(key)
                #kflow_read_app_progressapi_fast(self, key)
            else:
                log.warn("Skipping [" + key + "] since 'do_execute' is set to false.")
        return(True)

    # TO DO - add parameter filter_regex
    def upload_tables_to_remote(self, tbl_list = [], filter_contains = '', if_exists = "replace", create_primary_key = False):
        inspector = sqlalchemy.inspect(self.sqlite.ENGINE)
        df = pd.DataFrame(inspector.get_table_names())
        
        # By default, upload each table that meets the default criteria
        if(tbl_list == []):
            if(filter_contains !=''):
                tbl_list = df[df[0].str.contains(filter_contains)].iloc[:, 0].tolist()
            else:
                raise ValueError('Must provide either tbl_list or filter_contains to select the tables for upload')
        #tbl_list = df[df[0].str.contains('kissflow_awards_v1_headers')].iloc[:, 0].tolist()

        for table_name in tbl_list:
            print(table_name)
            with self.sqlite.ENGINE.begin() as cm:
                try:
                    df_for_import = pd.read_sql("select * from " + table_name, cm)
                except Exception as err:
                    df_for_import = pd.read_sql("select * from " + table_name, cm)
                    pass

            self.scm.importDF(df_for_import, table_name, if_exists = if_exists, create_primary_key = create_primary_key)
        #print(df_for_import.head())

    def combine_apps_to_one_table(self, sqlite=False):
        """ Create a single table with data from all of the apps """
        if(sqlite):
            kflow_eng = sqlalchemy.create_engine(**self.SCM_SQLITE)
        else:
            kflow_eng = sqlalchemy.create_engine(**self.SCM_TRUTH)

        # Get table list from database
        inspector = sqlalchemy.inspect(kflow_eng)
        table_names = inspector.get_table_names()
        table_names = [t for t in table_names if (('kissflow' in t) & (t!= 'kissflow_all_apps') & (t != 'kissflow_prv_kpis'))]

        log.info("Table names: " + ",".join(table_names))

        # Build SQL
        field_mappings = self.config['kissflow_apps']
        id_fields = ['id']
        mapped_fields = ['app_name', 'site_location', 'spend_amount', 'buyer', 'category', 'project', 'project_id']
        common_fields = [ 'process_name', 'process_step', 'assigned_to', 'subject', 'initiated_at', 'initiated_by', 'action_name', 'action_performed_datetime', 'action_performed_emailid', 'date_loaded', 'datetime_loaded']
        all_fields = id_fields + mapped_fields + common_fields
        q = ''
        app_name = ''
        i = 0
        for t in table_names:
            app_name_search = re.search('kissflow_(.+?)_headers', t)
            # If this table has kissflow app data
            if app_name_search:
                app_name = app_name_search.group(1)
                # If there is enough mapping info in the .config file to process this app's data
                if((app_name in field_mappings.keys()) & ('site_location_fieldname' in field_mappings[app_name].keys())):
                    
                    q_txt= " select id, '{appname}' as app_name, "
                    
                    # App-specific fields
                    
                    q_txt+= field_mappings[app_name]['site_location_fieldname'] + " as site_location,"
                    q_txt+= field_mappings[app_name]['spend_amount_fieldname'] + " as spend_amount,"
                    q_txt+= field_mappings[app_name]['buyer_fieldname'] + " as buyer,"
                    q_txt+= field_mappings[app_name]['category_fieldname'] + " as category,"
                    q_txt+= field_mappings[app_name]['project_fieldname'] + " as project,"
                    q_txt+= field_mappings[app_name]['project_id_fieldname'] + " as project_id,"
                    
                    q_txt+= ",{tablename}.".join(common_fields) # Common fields
                    
                    q_txt+= " ,status,percent_complete,completed_steps,skipped_steps,notstarted_steps,comments " # Progress table fields
                    
                    q_txt+=" from {tablename} left join {progress_table_name} on {tablename}.id =  {progress_table_name}.item_id "
                    # Precede this part of the SQL with 'UNION'?
                    if(i != 0):
                        q+= " UNION "
                    q+=q_txt.format(appname=app_name,tablename=t,progress_table_name=t.replace("_headers","_progress"))
                    i+=1
                else:
                    log.warn(" Table Not Included - Need Mappings for app " + app_name + " (table " + t + ")")

        log.debug(" SQL for combining apps into one table: " + q)
        
        # Drop combined table & create anew
        #Integer, primary_key = True
        meta = MetaData()
        kissflow_all_apps = Table(
           'kissflow_all_apps', meta,
           Column('pkid', Integer, primary_key=True, autoincrement=True),
           Column('id', String(255)), 
           Column('app_name',  String(255)), 
           Column('site_location',  String(255)), 
           Column('spend_amount', DECIMAL(16,2)), 
           Column('buyer',  String(255)), 
           Column('category',  String(255)), 
           Column('project',  String(255)), 
           Column('project_id',  String(255)), 
           Column('process_name',  String(255)), 
           Column('process_step',  String(255)), 
           Column('assigned_to',  String(255)), 
           Column('subject',  String(800)), 
           Column('initiated_at',  String(255)), 
           Column('initiated_by',  String(255)), 
           Column('action_name',  String(255)), 
           Column('action_performed_datetime', DateTime), 
           Column('action_performed_emailid',  String(255)), 
            
            # Progress fields
           Column('status', String(255)),
           Column('percent_complete', Float),
           Column('completed_steps', Text),
           Column('skipped_steps', Text),
           Column('notstarted_steps', Text),
           Column('comments', Text),

           Column('date_loaded', DateTime), 
           Column('datetime_loaded', DateTime)
        )
        kissflow_all_apps.drop(kflow_eng, checkfirst=True) # Drop if exists
        meta.create_all(kflow_eng) # Create Table
        
        # Load data
        q2 = "INSERT INTO kissflow_all_apps(" + ','.join(all_fields) + " ,status,percent_complete,completed_steps,skipped_steps,notstarted_steps,comments " + ") SELECT " + ",".join(all_fields) + " ,status,percent_complete,completed_steps,skipped_steps,notstarted_steps,comments " + " FROM (" + q + " ) ABC "
        #print(q2)
        with kflow_eng.begin() as con:
            r1 = con.execute(q2)

from . import kissflow_transforms
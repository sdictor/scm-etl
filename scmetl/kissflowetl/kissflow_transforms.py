# -*- coding: utf-8 -*-

"""

Module kissflow_transforms.py

"""
import logging as log

# SQL and data manipulation libraries
from .. import utils as dutils
from . import kissflow_helpers
from . import KissflowBatchJobs


import sqlalchemy
import pandas as pd
#import os

#from pandasql import sqldf as psqldf

import datetime
from datetime import timedelta
#import time
import numpy as np
from pathlib import Path


def prv_kpi_calcs(batchjobs, use_sqlite):
    """
        Calculate some metrics based on Kissflow PRV
    """
    # Database selection
    report_db = batchjobs.scm if use_sqlite == False else batchjobs.sqlite

    # Get PRV headers data
    with report_db.ENGINE.begin() as conn:
        df = pd.read_sql_query( "select * from kissflow_prv_headers",  conn)

    # Type cast
    df['dim_time_startdate'] = pd.to_datetime(df['initiated_at'], errors='coerce')
    df['metric_lead_time'] = df.apply(
            lambda row: dutils.days_diff(row['initiated_at'],row['date_needed']),
            axis=1
    )
    # Date dimensions
    df['dim_time_year_start'] = df.dim_time_startdate.dt.year.fillna(0).astype(int).astype(str)
    df['dim_time_quarter_start'] = df['dim_time_year_start'] + '-' + df.dim_time_startdate.dt.quarter.fillna(0).astype(int).astype(str)
    df['dim_time_month_start'] = df['dim_time_year_start']  + '-' +  df.dim_time_startdate.dt.month.fillna(0).astype(int).astype(str)

    # Binning
    def engagement_bins(n_days):
        if (n_days ==  None):
            return(None)
        elif (n_days > 90):
            return('e. Over 90 days')
        elif (n_days > 60):
            return('d. 61 - 90 days')
        elif (n_days > 30):
            return('c. 31 - 60 days')
        elif (n_days > 15):
            return('b. 15 - 30 days')
        else:
            return('a. <= 15 days')

    df['dim_bin_early_engagement'] = df.metric_lead_time.apply(engagement_bins)

    # Sort
    df = df.sort_values(by = ['category', 'metric_lead_time'])
    df = df[df.columns.sort_values()]

    # Choose & Rename fields
    cols_dim = df.filter(like='dim_').columns.sort_values()
    cols_met = df.filter(like='metric_').columns.sort_values()
    cols_others = ['id',
    'pr_number',
    'type_of_pr',
    'process_name',
    'process_step',
    'assigned_to',
    'subject',
    'initiated_at',
    'date_needed',
    'initiated_by',
    'action_name',
    'action_performed_datetime',
    'action_performed_emailid',
    'sitelocationenduse',
    'estimated_total_amount_in_php',
    'buyer',
    'category'
    ]

    cols_include =  cols_dim | cols_met | cols_others
    df_final = df[cols_include]

    df_final.rename(columns = {
            'sitelocationenduse': 'site_location',
            'estimated_total_amount_in_php':'spend_amount'
    }, inplace=True)

    create_primary_key = (not use_sqlite)  # Add primary key field if importing to mysql, otherwise hold off
    report_db.importDF(df_final, 'kissflow_prv_kpis', if_exists='replace', create_primary_key = create_primary_key)
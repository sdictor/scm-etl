import pandas as pd
from datetime import datetime
from pathlib import Path
import json
import re

def flatten_list(l):
    flat_list = list()
    for sublist in l:
        for item in sublist:
            flat_list.append(item)
    return(flat_list)

def sort_df_cols(df):
    return(df.reindex(sorted(df.columns), axis=1))

def extract_values(obj, key):
    """Pull all values of specified key from nested JSON."""
    # From https://hackersandslackers.com/extract-data-from-complex-json-python/
    arr = []
    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr
    results = extract(obj, arr, key)
    return results

# Function to test for date format
def test_df_date_format(dfcol, fmt):
    formatok = True
    try:
        pd.to_datetime(dfcol, format=fmt, errors='raise')
        # do something
    except ValueError:
        formatok = False
        pass
        
    return(formatok)
    
def convert_date(string):
    ALLOWED_DATE_FORMATS = ['%Y-%m-%d', '%Y/%m/%d', '%d.%m.%Y']
    for format in ALLOWED_DATE_FORMATS:
        try:
            return(datetime.strptime(string, format))
        except ValueError:
            pass

def convert_date_with_format(df, column_name):
    '''  https://realpython.com/fast-flexible-pandas/ ''' 
    return(pd.to_datetime(df[column_name], format='%d/%m/%y %H:%M'))

def lookup(s):
    ''' 
    From https://github.com/sanand0/benchmarks/blob/master/date-parse/date-parse.py
    This is an extremely fast approach to datetime parsing.
    For large data, the same dates are often repeated. Rather than
    re-parse these, we store all unique dates, parse them, and
    use a lookup to convert all dates.
    ''' 
    dates = {date:pd.to_datetime(date) for date in s.unique()}
    return(s.apply(lambda v: dates[v]))

def pd_convert_all_dates(df):
    #df.loc[:, df.columns.str.contains('date')] = pd.to_datetime(df.loc[:, df.columns.str.contains('date')])
    date_cols = [col for col in df if "date" in col]
    #time_cols = [col for col in df if "time" in col]
    #datetime_cols = list(set(date_cols) | set(time_cols))

    #filter_col = [date_cols for col in df if "date" in col]
    for col in date_cols:
        try:
            df[col] = pd.to_datetime(df[col])
        except Exception as err:
            pass

def pd_convert_all_times(df):
    #df.loc[:, df.columns.str.contains('date')] = pd.to_datetime(df.loc[:, df.columns.str.contains('date')])
    #date_cols = [col for col in df if "date" in col]
    time_cols = [col for col in df if "time" in col]
    #datetime_cols = list(set(date_cols) | set(time_cols))

    #filter_col = [col for col in df if "date" in col]
    for col in time_cols:
        try:
            df[col] = pd.to_datetime(df[col])
        except Exception as err:
            pass

def pd_missing_values(df):
    ''' Returns dataframe with % of missing data in each column '''
    columns = df.columns
    percent_missing = round(df.isnull().sum() / len(df),2)
    missing_value_df = pd.DataFrame({'column_name': columns,'percent_missing': percent_missing})
    return(missing_value_df)
    
def readCSV(file_path):
    ''' Reads CSV into dataframe and handles encoding of any unexpected characters '''
    df = pd.read_csv(file_path, 'r',errors='backslashreplace')
    return(df)

def readJSON(file_path):
    ''' Reads JSON into dataframe and handles encoding of any unexpected characters '''
    df = pd.read_json(file_path, 'r',errors='backslashreplace')
    return(df)

"""
    List of Characters to be excluded in field names
    This list is used by friendly_colnames_db
"""
delchars = ''.join(c for c in map(chr, range(256)) if ((not c.isalnum()) & (c!='_')))

def friendly_colnames_db(col_names):
    ''' Make list of field names database-safe with underscores for spaces and no special characters '''
    new_names = list()
    for c in col_names:
        nc = c.lower().strip().replace(" ", "_").replace("__", "_")
        nc = ''.join(ch for ch in nc if (not ch in delchars))
        new_names.append(nc[:40])
    return(new_names)

def cleanDFColnames(df):
    ''' Make dataframe column names friendly for database '''
    df.columns = friendly_colnames_db(list(df.columns))
    return(df)


def prefixColnames(df, prefix):
    ''' Add a prefix to dataframe column names '''
    new_names = prefix + df.columns
    df.columns=new_names

#def dedupeColnames(df):
#    # TO DO: Rewrite this function to make it work
#    cols=pd.Series(df.columns)
#    for dup in df.columns.get_duplicates(): cols[df.columns.get_loc(dup)]=[dup+'__________________.'+str(d_idx) if d_idx!=0 else dup for d_idx in range(df.columns.get_loc(dup).sum())]
#    df.columns=cols

def make_colnames_unique(df):
    df_columns = df.columns
    seen = set()
    new_names = list()
    for item in df_columns:
        fudge = 1
        newitem = item
        while newitem in seen:
            fudge += 1
            newitem = "{}_{}".format(item, fudge)
        #yield newitem
        new_names.append(newitem)
        seen.add(newitem)
    #return(new_names)
    df.columns = new_names
    
#def add_date_dimensions(df):
    #pr_processed['month'] = pr_processed['pr_approve_date'].dt.month.astype(str)
    #pr_processed['quarter'] = pr_processed['pr_approve_date'].dt.quarter.astype(str)

def loc_expand(df, loc):
    rows = []
    for i, row in df.iterrows():
        vs = row.at[loc]
        new = row.copy()
        for v in vs:
            new.at[loc] = v
            rows.append(new)

    return(pd.DataFrame(rows))

def iloc_expand(df, iloc):
    rows = []
    for i, row in df.iterrows():
        vs = row.iat[iloc]
        new = row.copy()
        for v in vs:
            row.iat[iloc] = v
            rows.append(row)

    return(pd.DataFrame(rows))

def unnest(df, col):
    unnested = (df.apply(lambda x: pd.Series(x[col]), axis=1)
                .stack()
                .reset_index(level=1, drop=True))
    unnested.name = col
    return df.drop(col, axis=1).join(unnested)

def rewrite_json_cols(df, json_cols):
    # conversion function:
    def dict2json(dictionary):
        return json.dumps(dictionary, ensure_ascii=False)
    # overwrite the dict column with json-strings    
    for col_name in json_cols:
        df[col_name] = df[col_name].map(dict2json)

# Identify columns that are list type in order to flag them as JSON type for error-free loading to the database
#def find_listtype_cols(df, search_n_rows = 100):
#    list_types = list()
#    for c in range(0,len(df.columns)-1):
#        is_list = False
#        for r in range(0,min(len(df),search_n_rows)):
#            is_list = ( isinstance(df.iloc[r][c], list) | isinstance(df.iloc[r][c], dict) )
#            if(is_list):
#                list_types.append(df.columns[c])
#                break
#    return(list_types)
def find_listtype_cols(df, search_n_rows = 100):
    str_cols = list(df.select_dtypes(exclude=['int','bool','float64','float']).columns)
    list_types = list()
    for c in str_cols:
        is_list = (sum(df[c].apply(lambda x: isinstance(x,list))) > 0)
        if(is_list):
            list_types.append(c)
        else:
            is_dict = (sum(df[c].apply(lambda x: isinstance(x,dict))) > 0)
            if(is_dict):
                list_types.append(c)
    return(list_types)

def anyDbImportDF(engine, df, target_table, if_exists = "fail"):
    ''' Import a pandas dataframe into the selected database '''
    ''' TO-DO: Log the transaction in a transactions log '''
    # Add date/time column to the dataframe
    df['date_loaded'] = datetime.now().date()
    df['datetime_loaded'] = datetime.now()

    conn_truth = engine.connect()
    # Note - AUTOCOMMIT not valid for SQLite databases
    #conn_truth = conn_truth.execution_options(isolation_level="AUTOCOMMIT")
    pd.io.sql.to_sql(frame=df, name=target_table, if_exists=if_exists,con=conn_truth, index=False)

    # Create primary key
    # ALTER TABLE purch_group_changes ADD pkid BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY;
    # ALTER TABLE purch_group_changes ADD pkid serial;

    # Add indices

    conn_truth = None

def get_create_table_string(tablename, connection):
    sql = """
    select * from sqlite_master where name = "{}" and type = "table"
    """.format(tablename) 
    result = connection.execute(sql)

    create_table_string = result.fetchmany()[0][4]
    return create_table_string

def add_pk_to_create_table_string(create_table_string, colname):
    regex = "(\n.+{}[^,]+)(,)".format(colname)
    return re.sub(regex, "\\1 PRIMARY KEY,",  create_table_string, count=1)

def add_pk_to_sqlite_table(tablename, index_column, connection):
    cts = get_create_table_string(tablename, connection)
    cts = add_pk_to_create_table_string(cts, index_column)
    template = """
    BEGIN TRANSACTION;
        ALTER TABLE {tablename} RENAME TO {tablename}_old_;

        {cts};

        INSERT INTO {tablename} SELECT * FROM {tablename}_old_;

        DROP TABLE {tablename}_old_;

    COMMIT TRANSACTION;
    """

    create_and_drop_sql = template.format(tablename = tablename, cts = cts)
    connection.executescript(create_and_drop_sql)


import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base

def super_bulk_insert(eng, tablename, df_source):
    # Testing new data upload mechanism
    tblname = tablename
    # 1. Create empty table in database
    with eng.begin() as con:
         pd.io.sql.to_sql(frame=df_source[0:0], name=tblname, if_exists='replace', con=con, index=False)

    # 1.5 Add primary key to table in database (caution: as written, will not work for SQLite)
    with eng.begin() as con:
        q_txt = "ALTER TABLE {tblname} ADD COLUMN pkid SERIAL PRIMARY KEY"
        rs = con.execute(q_txt.format(tblname=tblname))

    #2. Reflect the new data table to an ORM class
    with eng.begin() as conn:
        meta = sqlalchemy.MetaData()
        tbl_for_import = sqlalchemy.Table(tblname, meta, autoload=True, autoload_with=eng)

    #3. Bulk insert
    def orm_bulk_insert(eng, tablename, df_source):
        # create an unbound base our objects will inherit from
        Base = declarative_base()
        metadata = sqlalchemy.MetaData(bind=eng)
        Base.metadata = metadata
        g = globals()
        metadata.reflect()
        for tname, tableobj in metadata.tables.items():
            if(tablename in tname):
                g[tname] = type(str(tname), (Base,), {'__table__' : tableobj })
                #print("Reflecting {0}".format(tname))
        session = sqlalchemy.orm.sessionmaker(bind=eng)()
        #return Session()
        #session = reflect_table_to_declarative(eng_mysql, 'test_dataimport')

        #session.bulk_insert_mappings(g[tablename], df_source.to_dict(orient="records"))

        # See https://stackoverflow.com/questions/45484171/sqlalchemy-bulk-insert-is-slower-than-building-raw-sql
        session.execute(
            g[tablename].__table__.insert().values(df_source.to_dict(orient="records"))
        )

        print(g[tablename])
        print(len(df_source))
        session.commit()
        session.close()

    orm_bulk_insert(eng, tblname, df_source)


def try_parsing_date(
    text,
    formats = ['%Y-%m-%d %H:%M:%S.%f','%Y-%m-%d', '%m/%d/%Y %H:%M', '%m/%d/%Y', '%d.%m.%Y', '%d/%m/%Y']
):
    if isinstance(text, datetime):
        # Datetime object has been passed
        return (text)
    elif  isinstance(text, pd.Timestamp):
        # Timestamp object has been passed
        return (text.date())
    else:
        # String has been passed
        for fmt in formats:
            try:
                return (datetime.strptime(str(text).strip(), fmt))
            except ValueError:
                try:
                    return (datetime.strptime(str(text).strip()[0:10], '%Y-%m-%d'))
                except ValueError:
                    pass
                    print('Could not parse date ( ' + str(text) + ' ) ')
                return ''

def as_epoch(x):
    if isinstance(x, datetime):
        return((x.date() - datetime(1970, 1, 1).date()).total_seconds())
    else:
        x_as_date = try_parsing_date(x)
        if isinstance(x_as_date, datetime):
            return ((x_as_date.date() - datetime(1970, 1, 1).date()).total_seconds())
        else:
            return(None)

def days_diff(start,end):
    """ Difference in days between two datetimes """
    if((start == None) | (end == None)):
        result = None
    else:
        result = (as_epoch(end) - as_epoch(start)) / 86400
    return(result)


import os, fnmatch
def find_file_first_match(pattern, path):
    """ Search a directory & subdirectories for a file name based on pattern and return the first match only """
    result = []
    for root, dirs, files in os.walk(path):
    #for sorted(os.listdir('path'),key=os.path.getctime):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                #result.append(os.path.join(root, name))
                return(os.path.join(root, name))


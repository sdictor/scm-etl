# -*- coding: utf-8 -*-

"""Top-level package for scm_py_data_utils."""

__author__ = 'Stephen Dictor'
__email__ = 'sdictor@renew-ap.com'
__version__ = '0.1.0'


"""
Class ReportDatabase
"""
import pandas as pd
from pandas.io import sql
import pymysql
import sqlalchemy
from sqlalchemy import Table, Integer, Column, MetaData
from datetime import datetime
import requests
import json
from . import utils as dutils
import csv
import urllib.parse

class ReportDatabase:
    def __init__(self, **kwargs):
        ''' Constructor for this class. '''
        # Defaults
        self.GOOGLE_SECRET = ''
        self.DB_CONN_STRING = {}
        self.ENGINE = None
        #self.KISSFLOW_SECRET = {'user': '', 'key': '' }
        
        # Parameter values
        self.__dict__.update(kwargs)
        
        # Initialize engine
        if(self.DB_CONN_STRING != {}):
            self.ENGINE = sqlalchemy.create_engine(**self.DB_CONN_STRING)

        # Set pandas options
        pd.options.display.max_rows = 400
        pd.options.display.max_columns = 999

    def getDBConnString(self):
        ''' Connection string for SQL Alchemy in the format of "mysql+mysqldb://[url]:[password]@[host]:[port]"  '''
        return(self.DB_CONN_STRING)

    def setDBConnString(self, conn_string):
        ''' Connection string for SQL Alchemy in the format of "mysql+mysqldb://[url]:[password]@[host]:[port]"  '''
        self.DB_CONN_STRING = conn_string
        self.ENGINE = sqlalchemy.create_engine(**self.DB_CONN_STRING)
        #eng = sqlalchemy.create_engine(**self.DB_CONN_STRING,encoding='latin1',echo=False,convert_unicode=False)


    def setKissflowSecret(self, kissflow_secret):
        self.KISSFLOW_SECRET = kissflow_secret

    def getKissflowSecret(self):
        return(self.KISSFLOW_SECRET)

    def setGoogleSecret(self, google_secret):
        self.GOOGLE_SECRET = google_secret

    def getGoogleSecret(self):
        return(self.GOOGLE_SECRET)

    def runSQL(self, q):
        ''' Run a SQL command '''
        #engine = sqlalchemy.create_engine(self.DB_CONN_STRING)
        conn = self.ENGINE.connect()
        #conn = conn.execution_options(isolation_level="AUTOCOMMIT")
        rs = conn.execute(q)
        #for row in rs:
        #    print row
        conn = None

    def selectQuery(self, q):
        ''' Run a SELECT query and return results as pandas dataframe '''
        #engine = sqlalchemy.create_engine(self.DB_CONN_STRING)
        #conn = self.ENGINE.connect()
        #conn = conn.execution_options(isolation_level="AUTOCOMMIT")
        #df =  pd.read_sql_query(q, conn)
        #conn = None
        with self.ENGINE.begin() as conn:
            df =  pd.read_sql_query(q, conn)
        return(df)

    # TODO - update this function to handle creation of indices
    def importDF(self, df, target_table, if_exists = "fail", create_primary_key=False):
        ''' Import a pandas dataframe into the database '''
        ''' TO-DO: Log the transaction in a transactions log '''        
        # Add date/time column to the dataframe
        df['date_loaded'] = datetime.now().date()
        df['datetime_loaded'] = datetime.now()
        
        # Create table in db with primary key, and then append
        # TO-DO : Throw error if 'if_exists' is set to 'append', since the primary key creation will cause the original table to be replaced
        if(create_primary_key):
            # Delete pkid from import table, if it exists
            if("pkid" in df.columns):
                df.drop(columns=['pkid'], inplace=True)
            # Get Table Schema from pandas dataframe
            with self.ENGINE.begin() as con:
                db = pd.io.sql.SQLDatabase(con)
                tbl = pd.io.sql.SQLTable(name=target_table+'_temp',frame=df,pandas_sql_engine=db,index=False)

            # Modify the schema & make a new Table
            l = [Column(a.name, a.type) for a in tbl.table.columns if a.name != 'Index']
            l.insert(0,Column('pkid', Integer, primary_key=True, autoincrement=True))
            metadata = MetaData()
            new_table = Table(target_table, metadata, *l)

            # Drop & Create the table in the database
            new_table.drop(self.ENGINE, checkfirst=True) # Drop if exists
            metadata.create_all(self.ENGINE) # Create Table
            if_exists = 'append'

        #engine = sqlalchemy.create_engine(self.DB_CONN_STRING)
        with self.ENGINE.begin() as con:
            # Note - AUTOCOMMIT not valid for SQLite databases
            #conn_truth = conn_truth.execution_options(isolation_level="AUTOCOMMIT")
            #pd.io.sql.to_sql(frame=df, name=target_table, if_exists=if_exists,con=con, index=False)
            df.to_sql(target_table, con, if_exists=if_exists, index=False)

        # Create primary key
        # ALTER TABLE scm_truth.purch_group_changes ADD pkid BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY;

    def scrubCSV(self, source_file, output_file, colname_prefix = ''):
            """ Scrubs CSV & writes back to another one """
            #df = pd.read_csv(open(source_file, 'r',errors='backslashreplace'))
            #df = pd.read_csv(open(source_file, 'r',errors='ignore'))
            df = pd.read_csv(open(source_file, 'r',encoding='ISO-8859-1'))

            # Scrub
            dutils.cleanDFColnames(df)
            df = dutils.sort_df_cols(df)
            # Get the right datatypes so everything is not a String
            dutils.pd_convert_all_dates(df)
            df = df.infer_objects()
            if(colname_prefix != ''):
                dutils.prefixColnames(df, colname_prefix)
            dutils.make_colnames_unique(df)

            # Delete all newline characters
            df.replace(to_replace=[r"\\t|\\n|\\r", "\t|\n|\r"], value=["",""], regex=True, inplace=True)

            # Write to file
            df.to_csv(output_file, sep='\t', quoting = csv.QUOTE_NONNUMERIC, encoding='utf-8', index=False)
            return(True)

    def importCSV(self, source_file, target_table, colname_prefix = '',create_primary_key=True):
            # Read data
            df = pd.read_csv(open(source_file, 'r',errors='backslashreplace'))

            # Scrub
            dutils.cleanDFColnames(df)
            df = dutils.sort_df_cols(df)
            # Get the right datatypes so everything is not a String
            dutils.pd_convert_all_dates(df)
            df = df.infer_objects()
            if(colname_prefix != ''):
                dutils.prefixColnames(df, colname_prefix)
            dutils.make_colnames_unique(df)
            print(str({ 'table': target_table, 'nrows':len(df)}))
            print('done  massaging')

            # Backup the table, if the target table already exists
            target_exists = True
            if(self.table_exists(target_table)):
                archive_table = "archive_" + target_table + "_" + datetime.now().strftime('%Y%m%d')
                print("new table: " + target_table + ", archive table: " + archive_table)

                self.runSQL("CREATE TABLE IF NOT EXISTS " + archive_table + " AS SELECT * FROM " + target_table + " WHERE 0 ")
                try:
                    self.runSQL("INSERT INTO " + archive_table + " SELECT * FROM " + target_table + " WHERE NOT EXISTS (SELECT * FROM " + archive_table + " );")
                except Exception as err:
                    pass

            # Load to database
            self.importDF(df, target_table, if_exists="replace", create_primary_key = create_primary_key)
            return(True)

    def importJSON(self, json_data, target_table, if_exists = "fail"):
        ''' Import a JSON data into the database as one field in a table '''
        # Add date/time column to the dataframe
        df = pd.DataFrame({
            'json_data' : [json_data],
            'date_loaded' : [datetime.now().date()],
            'datetime_loaded' : [datetime.now()]
        })
        #engine = sqlalchemy.create_engine(self.DB_CONN_STRING)
        conn_truth = self.ENGINE.connect()
        conn_truth = conn_truth.execution_options(isolation_level="AUTOCOMMIT")
        sql.to_sql(frame=df, name=target_table, if_exists=if_exists,con=conn_truth, index=False, dtype={'json_data': sqlalchemy.types.JSON})
        #sql.to_sql(frame=df, name=target_table, if_exists=if_exists,con=conn_truth, index=False)
        conn_truth = None

    def previewTable(self, source_table, n_rows = 10):
        ''' Return pandas dataframe with first n rows of a database table '''
        q = "SELECT * FROM " + source_table + " LIMIT " + str(n_rows)
        return(self.selectQuery(q))

    def listTables(self):
        #''' Return pandas dataframe of the tables in the database under the scm_truth schema '''
        #q = "SELECT TABLE_SCHEMA, TABLE_NAME, TABLE_ROWS, CREATE_TIME, TABLE_COMMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'scm_truth' AND TABLE_TYPE = 'BASE TABLE'"
        inspector = sqlalchemy.inspect(self.ENGINE)
        table_names = inspector.get_table_names()
        #return(self.selectQuery(q))
        return(table_names)


    def table_exists(self, table_name):
        conn = self.ENGINE.connect()
        ret = self.ENGINE.dialect.has_table(conn, table_name)
        print('Table "{}" exists: {}'.format(table_name, ret))
        conn.close()
        return ret

"""Example for reflecting database tables to ORM objects

This script creates classes for each table reflected
from the database.

Note: The class names are imported to the global namespace using
the same name as the tables. This is useful for quick utility scripts.
A better solution for production code would be to return a dict
of reflected ORM objects.

Example usage:

# set to database credentials/host
CONNECTION_URI = "postgres://..."

session = reflect_all_tables_to_declarative(CONNECTION_URI)

# do something with the session and the orm objects
results = session.query(some_table_name).all()

"""
import sqlalchemy
from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


def reflect_all_tables_to_declarative(uri):
    """Reflects all tables to declaratives
    https://charleslavery.com/notes/sqlalchemy-reflect-tables-to-declarative.html
    Given a valid engine URI and declarative_base base class
    reflects all tables and imports them to the global namespace.

    Returns a session object bound to the engine created.
    """
    # create an unbound base our objects will inherit from
    Base = declarative_base()

    engine = create_engine(uri)
    metadata = MetaData(bind=engine)
    Base.metadata = metadata

    g = globals()

    metadata.reflect()

    for tablename, tableobj in metadata.tables.items():
        g[tablename] = type(str(tablename), (Base,), {'__table__' : tableobj })
        print("Reflecting {0}".format(tablename))

    Session = sessionmaker(bind=engine)
    return Session()


# REFLECT EXISTING TABLE SCHEMA
def reflect_one_table(tablename, uri):
    eng = sqlalchemy.create_engine(uri)
    conn = eng.connect()
    meta = sqlalchemy.MetaData()
    tbl = sqlalchemy.Table(tablename, meta, autoload=True, autoload_with=eng)
    conn.close()
    #print(repr(tbl))


def get_tablenames_like(is_like, eng):
    inspector = sqlalchemy.inspect(eng)
    table_names = inspector.get_table_names()
    table_names = [t for t in table_names if ((is_like in t))]
    return(table_names)
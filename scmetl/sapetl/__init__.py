# -*- coding: utf-8 -*-
"""
Module sapetl
"""

def main(args=None):
    """ Main entry point of sapetl """
    print("This is the SAP ETL module")
    return 0

if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()

import logging as log
from .. import reportdb
#from . import sap_transforms

import json
import pandas as pd
import requests
import pandas as pd
from datetime import datetime
import time
import os, sys
import requests
from requests import HTTPError
from pathlib import Path
import sqlalchemy
from .. import utils as dutils
from . import google as goog
import dask # Dask library for making DAGs and multi threading
from dask import delayed, compute
from collections import OrderedDict

class SAPBatchJobs:
    """"
        Define parameters such as database connection and persist meta data in memory
    """
    def __init__(self, user_parameters):
        
        # Load User Parameters (Dict)
        self.user_parameters = user_parameters
        #self.conn_str = ''

        # Load Configuration Parameters
        config_var = os.environ.get('SCMETL_CONFIG', None)
        config_json = os.environ.get('SCMETL_CONFIG_JSON', None)
        config_file = os.environ.get('SCMETL_CONFIG_FILE', None)
        if(config_var != None):
            self.config = config_var
        elif(config_json != None):
            self.config = json.loads(config_json)
        elif(config_file != None):
            with open(config_file,'r') as f:
                self.config = json.load(f)
        else:
            raise LookupError("It is necessary to set the SCMETL_CONFIG_JSON or SCMETL_CONFIG_FILE environmental variable before running this script.")

        # DB connections & Source Files
        self.mysql_con_string = self.config['mysql_con_string'].format(yyyymmdd=self.user_parameters['yyyymmdd'])
        self.sqlite_con_string = self.config['sqlite_con_string'].format(yyyymmdd=self.user_parameters['yyyymmdd'])

        ''' Apply template to 1) file paths & 2) database connection strings by formatting string values in the inner dictionary
            This makes it possible to update database names that have a yyyymmdd timestamp.
        '''
        def format_nested_dict(nd):
            results = nd
            for k, level2 in nd.items():
                for k2, v2 in level2.items():
                    if(isinstance(v2,str)):
                        results[k][k2] = v2.format(yyyymmdd=self.user_parameters['yyyymmdd'])
            return(results)
        
        self.config['db_conns'] = format_nested_dict(self.config['db_conns'])
        self.config['import_files'] = format_nested_dict(self.config['import_files'])
        self.db_conns = format_nested_dict(self.config['db_conns'])
        self.import_files = format_nested_dict(self.config['import_files'])

        self.config['source_file_path'] = self.config['source_file_path'].format(yyyymmdd=self.user_parameters['yyyymmdd'])
        self.source_file_path = self.config['source_file_path'].format(yyyymmdd=self.user_parameters['yyyymmdd'])
        self.df_poitems_in = None
        self.df_poitems_out = None
        self.df_purch_changes = None

        # Meta data for renaming SAP fields
        df_ren = pd.read_csv(Path(__file__).parents[1] / "assets" / "field_renaming_map.csv")
        self.lookup_renaming_map = {_row['old_name']: _row['new_name'] for _row in df_ren.to_dict(orient='records')}

        # Meta data required for ETLs - these lookup tables to be loaded when user calls the corresponding load functions
        self.lookup_pgroup_buyers = None
        self.lookup_pgroup_buyer_location = None
        self.lookup_pgroup_category = None
        self.lookup_plantcodes_bu = None
        self.lookup_store_loc = None
        self.lookup_pr_series = None
        self.lookup_pr_series_icon = None

        # Kissflow data required
        self.df_kissflow_awards = None
        self.df_kissflow_prvs = None

        # MySql
        #self.mysql_con_string = "mysql+pymysql://scm_truth:juliavargas632@greenfuturo.com:3306/scm_truth"
        self.mysql_eng = sqlalchemy.create_engine(self.mysql_con_string)
        self.mysql_scm = reportdb.ReportDatabase()
        self.mysql_scm.setDBConnString({"name_or_url": self.mysql_con_string})

        # SQLite
        #self.sqlite_con_string = 'sqlite:///D:\\data_repo\\data_repo\\raw\\sap\\20190320\\kpis2.db'
        self.sqlite_eng = sqlalchemy.create_engine(self.sqlite_con_string)
        self.sqlite_scm = reportdb.ReportDatabase()
        self.sqlite_scm.setDBConnString({"name_or_url": self.sqlite_con_string})

        # List of the transforms that are available (must be accurate for the run_transforms and transform functions to work properly)
        self.transforms=OrderedDict([
            ('category_lookups', sap_transforms.category_lookups),
            ('calc_H00',sap_transforms.calc_H00),
            ('goods_services', sap_transforms.goods_services),
            ('typecast_dates', sap_transforms.typecast_dates),
            ('is_pr_outstanding', sap_transforms.is_pr_outstanding),
            ('po_processing_status',sap_transforms.po_processing_status),
            ('po_completion_status', sap_transforms.po_completion_status),
            ('pr_aging', sap_transforms.pr_aging),
            ('po_aging', sap_transforms.po_aging),
            ('pr_po_age_bins', sap_transforms.pr_po_age_bins),
            ('date_dimensions', sap_transforms.date_dimensions),
            ('foreign_local', sap_transforms.foreign_local),
            ('emergency_longform_routine', sap_transforms.emergency_longform_routine),
            ('sla_days_PR2PO' , sap_transforms.sla_days_PR2PO),
            ('spend_amount_bins', sap_transforms.spend_amount_bins),
            ('po_due_target', sap_transforms.po_due_target),
            ('delivery_due_target', sap_transforms.delivery_due_target),
            ('actual_vs_po_release_target', sap_transforms.actual_vs_po_release_target),
            ('actual_vs_delivery_target', sap_transforms.actual_vs_delivery_target),
            ('incoterms', sap_transforms.incoterms),
            ('io_number', sap_transforms.io_number),
            ('io_mappings', sap_transforms.io_mappings),
            ('rename_columns', sap_transforms.rename_columns),
            ('join_kissflow', sap_transforms.join_kissflow)
        ])
        # Note: rename_columns was before po_due_target

    def download_csvs_from_google_drive(self):
        # Download CSVs from google drive based on config file parameters
        gparams = self.config['google_drive_parameters']
        gauth = goog.get_drive_credentials(gparams['secret_file'])
        goog.download_googledrive_files(
            gauth = gauth, 
            drive_data_folder=gparams['drive_data_folder'],
            team_drive=gparams['team_drive'],
            file_search_string=self.user_parameters['yyyymmdd'],
            destination = self.config['source_file_path']
        )

    def import_each_csv(self, file_info, destination = 'sqlite'):
        # f is a dict with information about the file requiring import.
            # Destination database
            eng = self.mysql_eng if destination == 'mysql' else self.sqlite_eng
            reportdb = self.mysql_scm if destination == 'mysql' else self.sqlite_scm
            
            # Read data
            input_file = Path(self.source_file_path) / file_info['file_name']
            log.info('reading ' + str(input_file))
            df = pd.read_csv(open(input_file, 'r',errors='backslashreplace'))

            # Scrub
            dutils.cleanDFColnames(df)
            df = dutils.sort_df_cols(df)
            #dutils.pd_convert_all_dates(df)
            #df = df.infer_objects()
            dutils.prefixColnames(df,file_info['colname_prefix'])
            dutils.make_colnames_unique(df)
            log.info(str({ 'table': file_info['table_name'], 'nrows':len(df)}))
            log.info('done  massaging')

            # ** Create 'ID' columns for PO-Item **
            #if ('po_col' in file_info.keys()):
                #log.info(file_info['table_name'] + ": Creating PO-Item key fields")
                #df[file_info['colname_prefix'] + 'id_po_no'] = df[file_info['colname_prefix'] + file_info['po_col']]
                #pd.to_numeric(df[file_info['colname_prefix'] + 'id_po_no'], errors='coerce')
                #df[file_info['colname_prefix'] + 'id_po_no'] = df[file_info['colname_prefix'] + 'id_po_no'].astype('int64').apply(lambda x: str(x)[0:10])
                #df[file_info['colname_prefix'] + 'id_po_item_no'] = df[file_info['colname_prefix'] + file_info['poitem_col']].apply(lambda x: str.zfill(str(int(x)),5))

            # ** Create 'ID' columns for PR-Item  **
            #if ('pr_col' in file_info.keys()):
                #log.info(file_info['table_name'] + ": Creating PR-Item key fields")
                #df[file_info['colname_prefix'] + 'id_pr_no'] = df[file_info['colname_prefix'] + file_info['pr_col']].astype('int64').apply(lambda x: str(x)[0:10])
                #df[file_info['colname_prefix'] + 'id_pr_item_no'] = df[file_info['colname_prefix'] + file_info['pritem_col']].apply(lambda x: str.zfill(str(int(x)),5))

            # Backup the table, if the target table already exists
            new_table = file_info['table_name']

            with eng.begin() as con:
                table_exists = eng.dialect.has_table(con, file_info['table_name'])

            if(table_exists):
                archive_table = "archive_" + file_info['table_name'] + "_" + datetime.now().strftime('%Y%m%d')
                print("new table: " + new_table + ", archive table: " + archive_table)

                with eng.begin() as con:
                    con.execute("CREATE TABLE IF NOT EXISTS " + archive_table + " AS SELECT * FROM " + new_table + " WHERE 0 ")
                try:
                    with eng.begin() as con:
                        con.execute("INSERT INTO " + archive_table + " SELECT * FROM " + new_table + " WHERE NOT EXISTS (SELECT * FROM " + archive_table + " );")
                except Exception as err:
                    pass

            # Load to database
            reportdb.importDF(df, file_info['table_name'], if_exists="replace")
            return(True)

    def import_csvs(self, destination = 'sqlite'):
        """ 1) Load the CSVs into SQLite, 2) Rename the fields, 3) add ID columns """

        output = []
        for k, f in self.import_files.items():
           self.import_each_csv(f, destination=destination)
        
        # Rename all fields in table
        for k, f in self.import_files.items():
            sap_migrations.upgrade_tbl(self.sqlite_eng, f['table_name'], f['colname_prefix'])

        # Update table ID columns of each imported table
        sap_helpers.update_id_cols(self.import_files, self.sqlite_scm)

    # Get lookup tables from the SQLite database
    def load_lookup_tables(self):
        from sqlalchemy.orm import sessionmaker

        inspector = sqlalchemy.inspect(self.sqlite_eng)
        df = pd.DataFrame(inspector.get_table_names())
        tbl_list = df[df[0].str.contains('lookup')].iloc[:, 0].tolist()

        metadata = sqlalchemy.MetaData()
        metadata.reflect(bind=self.sqlite_eng)
        ses_maker = sessionmaker(bind=self.sqlite_eng)
        session = ses_maker()

        q1 = session.query(metadata.tables['lookup_buyer_codes']).all()
        self.lookup_pgroup_buyers = {_row.pgroup: _row.buyer for _row in q1}
        self.lookup_pgroup_buyer_location = {_row.pgroup: _row.buyer_location for _row in q1}
        self.lookup_pgroup_category = {_row.pgroup: _row.spendcube_scm_category for _row in q1}

        self.lookup_plantcodes_bu = {_row.plant_code: _row.bu_intended for _row in session.query(metadata.tables['lookup_plant_codes']).all()}
        self.lookup_store_loc = {_row.sloc: _row.slocdescription for _row in session.query(metadata.tables['lookup_store_loc']).all()}
        self.lookup_pr_series = {_row.pr_series: str(_row.pr_series) + ' - ' + str(_row.text) for _row in session.query(metadata.tables['lookup_pr_series']).all()}
        self.lookup_pr_series_icon = {_row.pr_series: _row.pr_series_icon for _row in session.query(metadata.tables['lookup_pr_series']).all()}
        
        q2 = session.query(metadata.tables['lookup_io_l2']).all()
        self.lookup_io_l2 = {_row.io: _row.l2_department for _row in q2}
        self.lookup_io_description = {_row.io: _row.io_description for _row in q2}
        
        q3 = session.query(metadata.tables['lookup_unspsc_codes']).all()
        self.lookup_unspsc = {_row.mat_group: _row.family_group for _row in q3}
        
        session.close()

    def load_purch_group_changes_sqlite(self):
        # Load purchase group changes from SQLite
        self.df_purch_changes = self.sqlite_scm.selectQuery("select * from purch_group_changes")
        dutils.pd_convert_all_dates(self.df_purch_changes)        
        dutils.pd_convert_all_times(self.df_purch_changes)        

    def upload_purch_group_changes_to_remote(self):
        # Load purchase group changes from SQLite
        self.mysql_scm.importDF(self.df_purch_changes, 'purch_group_changes', if_exists='replace', create_primary_key = True)

    def get_latest_purch_group_changes(self):
        # Make sure lookup tables are available
        if(self.lookup_pgroup_buyers == None):
            self.load_lookup_tables()
        # Get latest purch group changes from MySql db
        con2 = self.mysql_eng.connect()
        #con2 = sqlalchemy.create_engine(**db_conns['edc_redshift_sqla'])

        df_purch_changes = pd.read_sql_query("select * from purch_group_changes", con2)
        #order by pr, item, assignment_start_time"
        con2.close()

        # Scrub the purchase group changes, including putting the ID fields into standard format
        dutils.cleanDFColnames(df_purch_changes)
        df_purch_changes.sort_values(by=['pr','item','assignment_start_time'], inplace=True)
        df_purch_changes['item'] = df_purch_changes.item.apply(lambda x: str.zfill(str(x),5))
        df_purch_changes['pg_id_pr_item'] = df_purch_changes['pr'] + '_' + df_purch_changes['item']

        df_purch_changes['buyer'] = df_purch_changes['assignment_code'].map(self.lookup_pgroup_buyers)

        self.df_purch_changes = df_purch_changes

    def get_kissflow_data(self):
        self.df_kissflow_awards = self.mysql_scm.selectQuery(
            """
            select prio_number as kawards_pr_number, basis_of_award as dim_kissflow_awards_basis, id as id_kissflow_awards from kissflow_awards_headers
            where length(ltrim(rtrim(prio_number))) >= 10 and prio_number IS NOT NULL    
        """)

        self.df_kissflow_prvs = self.mysql_scm.selectQuery("""
        select pr_number as kprv_pr_number, id as id_kissflow_prv, type_of_pr as dim_kissflow_pr_type from kissflow_prv_headers
        where length(ltrim(rtrim(pr_number))) >= 10 and pr_number IS NOT NULL
        """)

        # Save to SQLite database
        #self.sqlite_scm.importDF(df_kissflow_awards, "kissflow_awards", if_exists="replace")
        #self.sqlite_scm.importDF(df_kissflow_prvs, "kissflow_prvs", if_exists="replace")

    def get_latest_purch_group_changes_from_redshift(self):
        # Make sure lookup tables are available
        if(self.lookup_pgroup_buyers == None):
            self.load_lookup_tables()

        engine = sqlalchemy.create_engine(**self.db_conns["edc_redshift_sqla"])
        conn = engine.connect()
        conn = conn.execution_options(isolation_level="AUTOCOMMIT")

        # This query uses the cdpos table
        # query = open(os.path.join(SQL_DIR,"purch_group_change_durations.sql"), 'r').read()

        # This query directly grabs data from the output table
        query = "select * from staging.pr_purch_group_changes"

        df_purch_changes = pd.read_sql_query(query, conn)
        conn.close()

        # PR CHANGE DATA: PROCESS (Adding some additional dimensions)
        import datetime
        import time
        import numpy as np

        df_purch_changes['PR'] = df_purch_changes['tabkey'].str.slice(3, 13)
        df_purch_changes['Item'] = df_purch_changes['tabkey'].str.slice(14, 18)
        df_purch_changes['assignment_start_time'] = pd.to_datetime(df_purch_changes['assignment_start_time'])
        df_purch_changes['assignment_end_time'] = pd.to_datetime(df_purch_changes['assignment_end_time'])
        df_purch_changes['start_time_epoch'] = [time.mktime(t.timetuple()) for t in df_purch_changes.assignment_start_time]
        df_purch_changes['end_time_epoch'] = [time.mktime(t.timetuple()) for t in df_purch_changes.assignment_end_time]
        df_purch_changes['duration_days'] = (df_purch_changes.end_time_epoch - df_purch_changes.start_time_epoch) / 86400
        df_purch_changes['pr_item_key'] = df_purch_changes['PR'] + df_purch_changes['Item']

        # Scrub the purchase group changes, including putting the ID fields into standard format
        dutils.cleanDFColnames(df_purch_changes)
        df_purch_changes.sort_values(by=['pr','item','assignment_start_time'], inplace=True)
        df_purch_changes['item'] = df_purch_changes.item.apply(lambda x: str.zfill(str(x),5))
        df_purch_changes['pg_id_pr_item'] = df_purch_changes['pr'] + '_' + df_purch_changes['item']
        df_purch_changes['buyer'] = df_purch_changes['assignment_code'].map(self.lookup_pgroup_buyers)

        # Save to SQLite database
        self.sqlite_scm.importDF(df_purch_changes, "purch_group_changes", if_exists="replace")
        
        # Update self (persist in memory for downstream ETL)
        self.df_purch_changes = df_purch_changes


    def download_lookup_tables_from_remote(self):
        inspector = sqlalchemy.inspect(self.mysql_eng)
        df = pd.DataFrame(inspector.get_table_names())
        tbl_list = df[df[0].str.contains('lookup')].iloc[:, 0].tolist()

        for table_name in tbl_list:
            with self.mysql_eng.begin() as cm:
                try:
                    df_for_import = pd.read_sql("select * from " + table_name, cm)
                except Exception as err:
                    df_for_import = pd.read_sql("select * from " + table_name, cm)
                    pass
            self.sqlite_scm.importDF(df_for_import, table_name, if_exists="replace")

    def init_join_sap_tables(self, destination = 'sqlite', me5a = False):
        """
            FIRST routine to load initial PR & PO item level data prior to subsequent
            data transformation tasks. Also known as the "Mega Merge" because  the first
            versions of this routine conduct a join of various SAP t-codes that have PR & PO item
            level data.

            Prior to invoking this method, the ME2M and ZMM csv files should be already
            loaded into the SQLite database and id_po_item, id_pr_item key fields should have
            been created and populated.
        """
        # Destination database
        eng = self.mysql_eng if destination == 'mysql' else self.sqlite_eng
        reportdb = self.mysql_scm if destination == 'mysql' else self.sqlite_scm

        # First bring tables into memory & de-duplicate ZMMPRSTAT
        tbl_me2m = None
        tbl_zmm_clean = None
        tbl_properties = None
        tbl_acct = None

        # Load tables from database into memory
        tbl_me2m = reportdb.selectQuery("select * from sap_me2m_basic") # where me2m_deletion_indicator is null
        #tbl_me2m = reportdb.selectQuery("select * from sap_me2m_basic")
        tbl_zmm = reportdb.selectQuery("select * from sap_zmmprstat")
        tbl_expd = reportdb.selectQuery("select distinct * from sap_expd")
        tbl_acct = reportdb.selectQuery("select * from sap_me2m_acct") # where acct_deletion_indicator is null

        tbl_zmm_clean = sap_helpers.dedupe_zmmprstat(tbl_zmm)
        # Typecast of date fields
        #dutils.pd_convert_all_dates(tbl_zmm_clean)
        log.info("zmm_clean # records: " + str(len(tbl_zmm_clean)))
        # TODO - VALIDATE ZMM DEDUPE ROUTINE
        self.sqlite_scm.importDF(tbl_zmm_clean, 'zmm_clean', if_exists='replace' )

        # Join ME2M and ZMMPRSTAT together
        me2m_zmm = pd.merge(tbl_me2m, tbl_zmm_clean, how='outer', left_on="me2m_id_po_item", right_on="zmm_id_po_item")
        # Join result with EXPD
        me2m_zmm_expd = pd.merge(me2m_zmm, tbl_expd, how='left', left_on="me2m_id_po_item", right_on="expd_id_po_item")
        # Join result with ME2M account
        me2m_zmm_expd_acct = pd.merge(me2m_zmm_expd, tbl_acct, how='left', left_on="me2m_id_po_item", right_on="acct_id_po_item")

        # Delete records with deletion indicators
        me2m_zmm_expd_acct = me2m_zmm_expd_acct.query('me2m_deletion_indicator.isnull() and acct_deletion_indicator.isnull()')


        # Final de-duplication
        # TODO - Update the code that removes duplicate entries
        combined_table = sap_helpers.dedupe(me2m_zmm_expd_acct,'me2m_id_po_item')
        #combined_table = me2m_zmm_expd_acct
        
        # Typecast of date fields
        #dutils.pd_convert_all_dates(me2m_zmm)

        #self.df_poitems_in = me2m_zmm.copy()
        #self.df_poitems_out = me2m_zmm
        self.df_poitems_in = combined_table.copy()
        self.df_poitems_out = combined_table
        log.info("Mega merge # records: " + str(len(self.df_poitems_in)))
        print("Mega merge # records: " + str(len(self.df_poitems_in)))
        
        return (True)

    # TRANSFORMS
    def undo_transforms(self):
        """ Undoes all of the transforms """
        self.df_poitems_out = self.df_poitems_in

    def run_all_transforms(self):
        """ Perform all of the ETL transforms in order """
        summary_html = ""
        summary_txt = ""

        #for k in self.transforms.keys():
        for k, v in self.transforms.items():
            sub_results = self.transform(k)
            #(self.transforms[k])(batchjobs=self)
            summary_txt += sub_results['summary_txt'] + '\n'
            summary_html += sub_results['summary_html']
        
        results = {'summary_txt':summary_txt, 'summary_html':summary_html}
        return(results)


    def transform(self, func_name):
        """ Function to dispatch a one of the transform functions on the batchjobs object """
        # TODO: Add timer of each process
        #try:
        #func = self.transforms[func_name]
        #a_transform = dask.delayed(func)(batchjobs=self)
        #a_transform.compute()
        
        # Columns prior to calculations
        before_fields = self.df_poitems_out.columns
        
        # Do the calculations
        (self.transforms[func_name])(batchjobs=self)
        
        # Display the fields added as a result of the calculations
        after_fields = self.df_poitems_out.columns
        new_fields = set(after_fields) - set(before_fields)
        summary_html = "<div><b>" + func_name + "</b><ul><li>"
        summary_html+="<li>".join(new_fields)
        summary_html+="</ul></div>"
        summary_txt = func_name + ": " + ",".join(new_fields)
        results = {'summary_txt':summary_txt, 'summary_html':summary_html}
        
        log.info(" New columns created: " + results['summary_txt'])
        return(results)

        #except KeyError:
        #    raise ValueError('The function ' + func_name + ' does not exist')
    
    # TO DO - add parameter filter_regex
    def upload_tables_to_remote(self, tbl_list = [], filter_contains = '', if_exists = "replace", create_primary_key = False):
        inspector = sqlalchemy.inspect(self.sqlite_eng)
        df = pd.DataFrame(inspector.get_table_names())
        
        # By default, upload each table that meets the default criteria
        if(tbl_list == []):
            if(filter_contains !=''):
                tbl_list = df[df[0].str.contains(filter_contains)].iloc[:, 0].tolist()
            else:
                raise ValueError('Must provide either tbl_list or filter_contains to select the tables for upload')
        #tbl_list = df[df[0].str.contains('kissflow_awards_v1_headers')].iloc[:, 0].tolist()

        for table_name in tbl_list:
            print(table_name)
            with self.sqlite_eng.begin() as cm:
                try:
                    df_for_import = pd.read_sql("select * from " + table_name, cm)
                except Exception as err:
                    df_for_import = pd.read_sql("select * from " + table_name, cm)
                    pass

            self.mysql_scm.importDF(df_for_import, table_name, if_exists = if_exists, create_primary_key = create_primary_key)
        #print(df_for_import.head())

def reload_scm_data_utils():
    from importlib import reload
    reload(du)
    reload(du.utils)
    import scm_py_data_utils as du


# Test MySql connection
def test_mysql_connection(sap_batch_jobs):
    return (sap_batch_jobs.mysql_scm.selectQuery("select count(*) as n_records from sap_purchase_items"))

from . import sap_helpers
from . import sap_transforms
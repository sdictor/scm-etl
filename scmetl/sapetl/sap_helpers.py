import sys

from .. import utils as dutils

import sqlalchemy, pymysql
from datetime import datetime
import pandas as pd
import numpy as np
import json
import os
from pandasql import sqldf as psqldf

# Dask library for making DAGs and multi threading
import dask
from dask import delayed, compute



def dedupe(df,key):
    """ Eliminate duplicate records of 'key' (such as id_pr_item) by keeping the record with the most non-blank fields """
    zmm = df.copy()
    dutils.make_colnames_unique( zmm )
    zmm['n'] = zmm.apply(lambda r: 1,1) # Count number of records per key
    dupe_pr_items = list(zmm.groupby(key).count().query("n > 1").reset_index()[key])
    df_dupes = zmm.query(key+" in ["+",".join(["'{}'".format(x) for x in dupe_pr_items])+"]")
    df_normals = zmm.query(key+" not in ["+",".join(["'{}'".format(x) for x in dupe_pr_items])+"]")
    #j.print_df(df_dupes)
    
    n_uniques = len(set(dupe_pr_items))
    n_dupes = len(df_dupes)
    
    print(f"# Duplicates: {n_uniques} unique items appear {n_dupes} times. {n_dupes - n_uniques} records to be removed.")
    
    print(f"# Normals: {len(df_normals)}")

    by_pr = df_dupes.groupby(key).apply(lambda x: json.loads(x.to_json(orient='records')))

    result = []
    for k,dupes in by_pr.iteritems():
        # See which of the records with duplicate pr item # has the most valid fields
        counts = [  len([a for a in dupe_row.values() if a is not None]) for dupe_row in dupes]
        i_to_keep = counts.index(max(counts))
        #dupes[i_to_keep]['id_pr_item'] = k
        dupes[i_to_keep][key] = k
        result.append(dupes[i_to_keep])
        #print( [  len([a for a in dupe_row.values() if a is not None]) for dupe_row in dupes])
    
    result_df = pd.DataFrame(result)

    df_final = pd.concat([df_normals, result_df],sort=True)
    print(f"# Final: {len(result_df)}")
    
    return df_final



def is_blank(x):
    if (pd.isna(x) | pd.isnull(x)):
        return(True)
    else:
        # Check for Numpy nan?  | np.isnan(x)
        # Check for empty string or None type
        return ( (str(x).strip() == '') | (x == None) )

def update_id_cols(import_tables, scm):
    q1_update = """
    update {table_name}
    set 
        {po_no_des} = cast((cast({po_no_source} as BIGINT)) as CHAR(10)),
        {po_item_no_des} = (case
                when length(cast(cast({po_item_source} as int) as CHAR(5))) >= 5 then cast(cast({po_item_source} as int) as CHAR(5))
                when length(cast(cast({po_item_source} as int) as CHAR(5))) = 4 then '0' || cast(cast({po_item_source} as int) as CHAR(5))
                when length(cast(cast({po_item_source} as int) as CHAR(5))) = 3 then '00' || cast(cast({po_item_source} as int) as CHAR(5))
                when length(cast(cast({po_item_source} as int) as CHAR(5))) = 2 then '000' || cast(cast({po_item_source} as int) as CHAR(5))
                when length(cast(cast({po_item_source} as int) as CHAR(5))) = 1 then '0000' || cast(cast({po_item_source} as int) as CHAR(5))
            else NULL
        end)
    """
    q2_update = """
        update {table_name} set {po_item_des} = {po_no_des} || '_' || {po_item_no_des};
    """
    for k, f in import_tables.items():
        # Update the PO id fields
        if ('po_col' in f.keys()):
            print(f['table_name'] + ": Creating PO-Item key fields")
            q1_update_filled = q1_update.format(
                               po_no_source= f['colname_prefix'] + f['po_col'],
                               po_item_source = f['colname_prefix'] + f['poitem_col'],
                               po_item_no_des = f['colname_prefix'] + 'id_po_item_no',
                               po_no_des = f['colname_prefix'] + 'id_po_no',
                               table_name = f['table_name']
                              )
            q2_update_filled = q2_update.format(
                               po_item_des = f['colname_prefix'] + 'id_po_item',
                               po_item_no_des = f['colname_prefix'] + 'id_po_item_no',
                               po_no_des = f['colname_prefix'] + 'id_po_no',
                               table_name = f['table_name']
                              )
            scm.runSQL(q1_update_filled)
            scm.runSQL(q2_update_filled)
        #Update the PR id fields
        if ('pr_col' in f.keys()):
            print(f['table_name'] + ": Creating PR-Item key fields")
            q1_update_filled = q1_update.format(
                               po_no_source= f['colname_prefix'] + f['pr_col'],
                               po_item_source = f['colname_prefix'] + f['pritem_col'],
                               po_item_no_des = f['colname_prefix'] + 'id_pr_item_no',
                               po_no_des = f['colname_prefix'] + 'id_pr_no',
                               table_name = f['table_name']
                              )
            q2_update_filled = q2_update.format(
                               po_item_des = f['colname_prefix'] + 'id_pr_item',
                               po_item_no_des = f['colname_prefix'] + 'id_pr_item_no',
                               po_no_des = f['colname_prefix'] + 'id_pr_no',
                               table_name = f['table_name']
                              )
            scm.runSQL(q1_update_filled)
            scm.runSQL(q2_update_filled)
#update_id_cols()

def dedupe_zmmprstat(df_zmm):
    """ De-duplicates a pandas dataframe representation of zmmprstat """
    # agg_cols = ['zmm_po_amount','zmm_pr_approved_date','zmm_po_approved_date','zmm_po_delivery_date','zmm_gr_uom','zmm_gr_date','zmm_gr_quantity']
    # List of non-aggregate columns to select: ",".join(df_zmm.drop(agg_cols, axis=1).columns)

    q = """
    SELECT   zmm_id_pr_item,
                zmm_id_pr_no,
                zmm_id_pr_item_no,
                zmm_id_po_item,
                zmm_id_po_no,
                zmm_id_po_item_no,
             zmm_material,
             zmm_material_description,
             zmm_pr_no ,
             zmm_po_num ,
             zmm_po_item_no,
             zmm_pr_qty ,
             zmm_pr_uom,
             zmm_pr_creation_date,
             zmm_pr_release_date,
             zmm_delivery_date,
             zmm_ideal_delivery_date,
             zmm_valuation_price,
             zmm_cost_center,
             zmm_currency1 as zmm_currency2,
             zmm_purch_grp ,
             zmm_created_by,
             zmm_pr_item_no,
             zmm_doc_type ,
             zmm_preq_processing_state ,
             zmm_requisitioner,
             zmm_plant   ,
             zmm_item_cat ,
             zmm_mat_grp  ,
             zmm_reservation_no,
             zmm_processing_status,
             zmm_order,
             zmm_asset             ,
             group_concat(distinct zmm_currency) as zmm_currency,
             group_concat(zmm_storage_location) AS zmm_storage_location,
             sum(zmm_po_amount)                                                                                 AS zmm_gr_amount_sum,
             max(zmm_pr_approved_date)                                                                          AS zmm_pr_approved_date_max,
             max(zmm_po_approved_date)                                                                          AS zmm_po_approved_date_max,
             min(zmm_po_delivery_date)                                                                          AS zmm_po_delivery_date_min,
             max(zmm_po_delivery_date)                                                                          AS zmm_po_delivery_date_max,
            min(zmm_gr_uom) as zmm_gr_uom,
            min(zmm_gr_date) as zmm_gr_date_min,
            sum(zmm_gr_quantity) as zmm_gr_quantity_total

    FROM     df_zmm
    where not(zmm_id_pr_item is null and zmm_id_po_item is null)
    GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,28,29
    """
    df_zmm_clean = psqldf(q, locals())
    return(df_zmm_clean)
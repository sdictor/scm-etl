"""

    Define output data schema

    (Migrations using SQLAlchemy's ORM))

    Add columns to the me2m, zmmprstat and expd tables

"""
# Dask library for making DAGs and multi threading
import dask
from dask import delayed, compute
from dask.highlevelgraph import HighLevelGraph

from sqlalchemy import Table, MetaData, Column, BigInteger, Integer, String, ForeignKey, Boolean, DateTime, CHAR
from sqlalchemy.ext.declarative import declarative_base

# SQLAlchemy migrate: pip install sqlalchemy-migrate
from migrate import *
import pandas as pd

def upgrade_tbl(migrate_engine, tblname, prefix):
    meta = MetaData(bind=migrate_engine)
    tbl = Table(tblname, meta, autoload=True)
    # Declare columns
    pkid = Column('id', BigInteger)

    id_po_no = Column(prefix + 'id_po_no', CHAR(10))
    id_po_item_no = Column(prefix + 'id_po_item_no', CHAR(5))
    id_po_item = Column(prefix + 'id_po_item', CHAR(16))

    id_pr_no = Column(prefix + 'id_pr_no', CHAR(10))
    id_pr_item_no = Column(prefix + 'id_pr_item_no', CHAR(5))
    id_pr_item = Column(prefix + 'id_pr_item', CHAR(16))

    # Create columns
    id_pr_no.create(tbl)
    id_pr_item_no.create(tbl)
    id_pr_item.create(tbl)

    # pkid.create(tbl, primary_key_name = 'id', index_name = 'idx_'+tblname)
    id_po_no.create(tbl)
    id_po_item_no.create(tbl)
    id_po_item.create(tbl)


# Create the additional columns on each of the tables
def task_upgrade_all_tables(nb_vars):
    for k, f in nb_vars.import_files.items():
        print(f['table_name'])
        upgrade_tbl(nb_vars.ENGINE, f['table_name'], f['colname_prefix'])
        # try:
        #    upgrade_tbl(ENGINE,f['table_name'], f['colname_prefix'])
        # except Exception as e:
        #    print("No update made for " + f['table_name'] + ':'+ str(e))
        #    pass

# %%
# Define the SAP-PO-Item-Properties Output table
from sqlalchemy import Column, BigInteger, Integer, String, ForeignKey, Boolean, DateTime, CHAR
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class sap_po_item_properties(Base):
    __tablename__ = "sap_po_item_properties"
    p_id = Column(BigInteger, primary_key=True, autoincrement=True)
    p_id_pr_no = Column(CHAR(10))
    p_id_pr_item_no = Column(CHAR(5))
    p_id_pr_item = Column(CHAR(16))

    p_id_po_no = Column(CHAR(10))
    p_id_po_item_no = Column(CHAR(5))
    p_id_po_item = Column(CHAR(16))

    p_att_is_emergency = Column(Integer)
    p_att_is_migration = Column(Integer)
    p_att_goods_or_services = Column(String)
    p_att_buyer_name = Column(String)
    p_att_bu_intended = Column(String)
    p_att_end_use_bu = Column(String)
    p_att_category = Column(String)
    p_att_sloc_desc = Column(String)
    p_status_is_pending_gr = Column(Integer)
    p_status_is_pr_released = Column(Integer)
    p_status_days_since_pr_created = Column(BigInteger)

# Create the destination table, if needed
# TODO - update this function so any columns that do not exist are added
def upgrade_destination_table(eng):
    try:
        sap_po_item_properties.__table__.create(bind=eng)
    except:
        print("Table probably exists already")
"""

Utility functions for Google Drive, Google CLoud, etc


"""

# Prepare to download the .csv files by doing Google Drive authentication & obtaining a list of the files to be downloaded.

# Depends on google-api-python-client

import os, pathlib
from oauth2client.client import GoogleCredentials
from oauth2client.service_account import ServiceAccountCredentials
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

# Necessary to use google-api-python-client library instead of PyDrive for file downloads from Team Drive
from apiclient import discovery
import httplib2

import logging

# Only critical log messages to avoid noise from Google APIs
logger = logging.getLogger(__name__)
logger.setLevel(logging.CRITICAL)

# Utility function for downloading files
import io
from apiclient.http import MediaIoBaseDownload

def get_drive_credentials(goog_secret_file):
    # Authentication using Google Service Account
    gauth = GoogleAuth()
    gauth.credentials = ServiceAccountCredentials.from_json_keyfile_name(goog_secret_file, ['https://www.googleapis.com/auth/drive'])
    return(gauth)

def download_file(drive_service, file_id, mimeType, filename):
    request = drive_service.files().get_media(fileId=file_id)
    fh = io.FileIO(filename, 'wb')
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        logger.info("Download %d%%." % int(status.progress() * 100))

def download_googledrive_files(gauth, drive_data_folder, team_drive, file_search_string, destination):
# PyDrive
    drive = GoogleDrive(gauth)
# Get list of files
    file_list = drive.ListFile({'q': "'" + drive_data_folder + "' in parents and trashed=false", 'corpora': "teamDrive", 'teamDriveId':team_drive,'includeTeamDriveItems': "true",'supportsTeamDrives': "true"}).GetList()
    latest_file_ids = [f['id'] for f in file_list if file_search_string in f['title']]
    latest_files_titles = [f['title'] for f in file_list if file_search_string in f['title']]
# Google API service
    drive_service = discovery.build('drive', 'v3', http=gauth.credentials.authorize(httplib2.Http()))
# Create folder
    if not os.path.exists(destination):
        os.mkdir(destination)

    for i in range(0,len(latest_files_titles)):
        logger.info("Downloading " + latest_files_titles[i])
        download_file(drive_service, latest_file_ids[i], 'text/csv', pathlib.Path(destination) / latest_files_titles[i])
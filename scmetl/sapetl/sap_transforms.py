# -*- coding: utf-8 -*-

"""

Module sap_transforms.py

"""
import logging as log

# SQL and data manipulation libraries
from .. import utils as dutils
from . import sap_helpers
from . import sap_migrations
from . import SAPBatchJobs


import sqlalchemy
import pandas as pd
#import os

# Dask library for making DAGs and multi threading
import dask
from dask import delayed, compute
#from dask.highlevelgraph import HighLevelGraph
#import graphviz
from pandasql import sqldf as psqldf

import datetime
from datetime import timedelta
#from datetime import datetime
#import time
import numpy as np
from pathlib import Path


def calc_H00(batchjobs):
    """ H00 is the stage where PRs are unassigned
    Ths 'H00' milestone refers to the date immediately afterwards, when PRs are assigned to a buyer
    """
    
    # Make sure df_purch_changes is loaded to memory
    if (isinstance(batchjobs.df_purch_changes,pd.DataFrame) == False):
        log.info("Purchase change history hasn't been loaded to memory. Attempting to load.")
        batchjobs.load_purch_group_changes_sqlite()
    elif (batchjobs.df_purch_changes.empty):
        log.warning("Purchase change history is empty.")

    df_h00 = batchjobs.df_purch_changes[batchjobs.df_purch_changes['assignment_code'] == 'H00']

    df_h00_time = df_h00.pivot_table(values='duration_days', index='pg_id_pr_item', fill_value=0, aggfunc='sum', margins=False, dropna=False)
    df_h00_time = df_h00_time.reset_index()

    # Timeline of Changes to Purchase Group Status
    df_h00_time['subchart_pgroup_hist_assign_start_times'] = df_h00.groupby('pg_id_pr_item').apply(lambda x: ','.join(x.assignment_start_time.astype(str)))
    df_h00_time['subchart_pgroup_hist_assign_end_times'] = df_h00.groupby('pg_id_pr_item').apply(lambda x: ','.join(x.assignment_end_time.astype(str)))
    df_h00_time['subchart_pgroup_hist_buyers'] = df_h00.groupby('pg_id_pr_item').apply(lambda x: ','.join(x.buyer))
    df_h00_time['subchart_pgroup_hist_durations'] = df_h00.groupby('pg_id_pr_item').apply(lambda x: ','.join(str(round(x.duration_days,1))))
    df_h00_time['time_H00_completed'] = df_h00.groupby('pg_id_pr_item').apply(lambda x: max(x.assignment_end_time))

    df_h00_time = df_h00_time[['pg_id_pr_item', 'time_H00_completed', 'duration_days',
    'subchart_pgroup_hist_assign_start_times','subchart_pgroup_hist_assign_end_times','subchart_pgroup_hist_buyers','subchart_pgroup_hist_durations'
    ]]
    df_h00_time.rename(columns={
        'duration_days':'metric_days_for_H00'
    }, inplace=True)

    #metric_days_for_H00
    #print(df_h00_time.head())

    batchjobs.df_poitems_out = pd.merge(batchjobs.df_poitems_out, df_h00_time, how='left', left_on='zmm_id_pr_item', right_on="pg_id_pr_item")
    return (True)

def category_lookups(batchjobs):
    # Make sure lookup tables are available
    if(batchjobs.lookup_pgroup_buyers == None):
        batchjobs.load_lookup_tables()

    # First, derive the PR series based on first digit of PR number
    batchjobs.df_poitems_out['dim_pr_series_no'] = batchjobs.df_poitems_out.zmm_id_pr_item.str[:1]

    # Apply 'VLookups' to the following dimensions
    batchjobs.df_poitems_out['dim_buyer_name'] = batchjobs.df_poitems_out['zmm_purch_grp'].map(batchjobs.lookup_pgroup_buyers)
    batchjobs.df_poitems_out['dim_buyer_location'] = batchjobs.df_poitems_out['zmm_purch_grp'].map(batchjobs.lookup_pgroup_buyer_location)
    batchjobs.df_poitems_out['dim_category'] = batchjobs.df_poitems_out['zmm_purch_grp'].map(batchjobs.lookup_pgroup_category)
    batchjobs.df_poitems_out['dim_end_use_bu'] = batchjobs.df_poitems_out['me2m_plant'].map(batchjobs.lookup_plantcodes_bu)
    batchjobs.df_poitems_out['dim_storage_loc'] = batchjobs.df_poitems_out['me2m_storage_location'].map(batchjobs.lookup_store_loc)
    batchjobs.df_poitems_out['dim_pr_series'] = batchjobs.df_poitems_out['dim_pr_series_no'].fillna(0).astype(int).map(batchjobs.lookup_pr_series)
    batchjobs.df_poitems_out['dim_pr_series_icon'] = batchjobs.df_poitems_out['dim_pr_series_no'].map(batchjobs.lookup_pr_series_icon)
    
    # Material group / UNSPSC
    #batchjobs.df_poitems_out['dim_unspsc_family'] = batchjobs.df_poitems_out['me2m_material_group'].map(batchjobs.lookup_unspsc)
    batchjobs.df_poitems_out['dim_unspsc_family'] = batchjobs.df_poitems_out['zmm_mat_grp'].map(batchjobs.lookup_unspsc)

def goods_services(batchjobs):
    """ Add the calculated field of goods or services """
    def xfm_goods_services(mat_group):
        """ Determine if an item is goods or services based on the material group """
        mat_group = str(mat_group)

        if (mat_group in ('FORCLNUP','ITM4DEPN')):
            return('GOODS')
        elif (mat_group in ('SRVCITMS','SERVICES')):
            return('SERVICES')
        elif ((mat_group == '') | (mat_group == None)):
            return('N/A')
        else:
            try:
                matgroup_int = int(mat_group)
            except:
                return('N/A')

            if(int(mat_group[0]) < 7):
                return('GOODS')
            elif(matgroup_int >= 99999999):
                return('NEW SERVICES')
            else:
                return('SERVICES')

    batchjobs.df_poitems_out['dim_goods_services'] = batchjobs.df_poitems_out['me2m_material_group'].apply(xfm_goods_services)

def typecast_dates(batchjobs):
    # Type cast the date fields
    date_fields = batchjobs.df_poitems_out.filter(like='date').columns    
    print(" Performing typecast of date fields: " + ",".join(date_fields))
    dutils.pd_convert_all_dates(batchjobs.df_poitems_out)
    #for d in date_fields:
        #batchjobs.df_poitems_out[d] = pd.to_datetime(batchjobs.df_poitems_out[d], errors='coerce')
    #    batchjobs.df_poitems_out['dt_'+d] = pd.to_datetime(batchjobs.df_poitems_out[d].apply(
    #        dutils.try_parsing_date
    #    ), errors='raise')
    
def typecast_dates2(batchjobs):
    # Type cast the date fields
    date_fields = batchjobs.df_poitems_out.filter(like='date').columns    
    print(" Performing typecast of date fields: " + ",".join(date_fields))
    #dutils.pd_convert_all_dates(batchjobs.df_poitems_out)
    for col in date_fields:
        try:
            # Try Month-Day-Year
            batchjobs.df_poitems_out[col] = pd.to_datetime(batchjobs.df_poitems_out[col].apply(
                lambda x: dutils.try_parsing_date(x,['%m/%d/%Y'])
            ), errors='raise')
            log.warning("Couldn't parse date as m/d/yyyy, attempting d/m/yyyy ...")
        except ValueError as err:
            try:
                # Try Day-Month-Year
                batchjobs.df_poitems_out[col] = pd.to_datetime(batchjobs.df_poitems_out[col].apply(
                    lambda x: dutils.try_parsing_date(x,['%d/%m/%Y'])
                ), errors='raise')
            except ValueError as err:
                log.warning(" Failed to parse date: Couldn't parse date as m/d/yyyy or d/m/yyyy.")          
                pass

    
    #for d in date_fields:
        #batchjobs.df_poitems_out[d] = pd.to_datetime(batchjobs.df_poitems_out[d], errors='coerce')
    #    batchjobs.df_poitems_out['dt_'+d] = pd.to_datetime(batchjobs.df_poitems_out[d].apply(
    #        dutils.try_parsing_date
    #    ), errors='raise')

def is_pr_outstanding(batchjobs):
    """ Add the calculated field of PR outstanding """
    def xfm_is_pr_outstanding(df_row):
        if(sap_helpers.is_blank(df_row['zmm_id_pr_item'])):
            """ There is no PR number  """
            return(False)
        if(isinstance(df_row['zmm_gr_date_min'], datetime.datetime) & (sap_helpers.is_blank(df_row['zmm_gr_date_min']) == False)):
            """ There have been Goods Receipts  """
            return(False)
        #if( not sap_helpers.is_blank(df_row['me2m_id_po_item'])):
        #    """ There is a PO Number """
        #    return(False)
        if( not sap_helpers.is_blank(df_row['zmm_po_approved_date_max'])):
            """ There is a PO approved date """
            #print(" PO approved date is " + str(type(df_row['zmm_po_approved_date_max'])) + str(df_row['zmm_po_approved_date_max']))
            return(False)
        
        #2/26/20 Addition - Migration PRs start with '7'
        if( df_row['zmm_id_pr_item'][0:1] == '7'):
            return(False)
        
        #print(" No PO approved date: " + str(type(['zmm_po_approved_date_max'])) + str(df_row['zmm_po_approved_date_max']))
        return(True)

    batchjobs.df_poitems_out['status_yesno_pr_outstanding'] = [xfm_is_pr_outstanding(row) for row in batchjobs.df_poitems_out.to_dict(orient='records')]


def po_completion_status(batchjobs):
    """ Add the calculated field of Open PO """

    """ First, cast both columns to float """
    #batchjobs.df_poitems_out['me2m_still_to_be_delivered_value'] = batchjobs.df_poitems_out['me2m_still_to_be_delivered_value'].fillna(0).astype(int)
    def col_to_numeric(col):
        batchjobs.df_poitems_out[col] = pd.to_numeric( batchjobs.df_poitems_out[col], errors='coerce')

    col_to_numeric('me2m_still_to_be_delivered_value')
    col_to_numeric('me2m_net_order_value')

    def xfm_po_completion_status(df_row):
        if (df_row['status_yesno_pr_outstanding']):
            """ If the PR is outstanding, then the PO is not outstanding (return False) """
            return ("PR is Outstanding")

        elif ((df_row['me2m_still_to_be_delivered_value'] > 0) & (
                df_row['me2m_still_to_be_delivered_value'] < df_row['me2m_net_order_value'])):
        #df_row['me2m_still_to_be_delivered_qty'] < df_row['me2m_order_quantity'])):
            """ Pending More Goods Receipts """
            return ("Open PO - Partial Goods Receipt")

        elif (df_row['me2m_still_to_be_delivered_value'] > 0):
            """ Pending Goods Receipt """
            return ("Open PO - Pending Goods Receipt")

        else:
            """ Completed """
            return ("No Pending Goods Receipts")

    batchjobs.df_poitems_out['status_po_completion_status'] = [xfm_po_completion_status(row) for row in batchjobs.df_poitems_out.to_dict(orient='records')]
    batchjobs.df_poitems_out['status_yesno_po_open'] = batchjobs.df_poitems_out['status_po_completion_status'].apply(lambda x: ('Open' in x))

def po_processing_status(batchjobs):
    """ Add the calculated field of PO Processing Status
        If there is a GR date or a PO approved date, then the PO is to be considered processed
    """
    batchjobs.df_poitems_out['status_yesno_po_processed'] = batchjobs.df_poitems_out.apply(
        lambda x: True if ((sap_helpers.is_blank(x['zmm_po_approved_date_max']) == False) | (sap_helpers.is_blank(x['zmm_po_approved_date_max']) == False)) else False,
        axis=1
    )


def pr_aging(batchjobs):
    """ Add the calculated field of PR age """
    dt_today = datetime.datetime.now().date()

    def xfm_pr_aging_end_date(df_row):
        pr_aging_end_date = dt_today if sap_helpers.is_blank(df_row['zmm_po_approved_date_max']) else df_row['zmm_po_approved_date_max']
        # Some PRs have completed but don't show an approved date. For these, capture the PO creation date
        pr_aging_end_date = df_row['me2m_document_date'] if sap_helpers.is_blank(pr_aging_end_date) else pr_aging_end_date
        return(pr_aging_end_date)

    #batchjobs.df_poitems_out['pr_aging_start_date'] = batchjobs.df_poitems_out.apply(
    #    lambda x: x['zmm_pr_creation_date'] if sap_helpers.is_blank(x['time_H00_completed']) else x['time_H00_completed'],
    #    axis = 1
    #)
    batchjobs.df_poitems_out['pr_aging_end_date'] = batchjobs.df_poitems_out.apply(xfm_pr_aging_end_date, axis=1)
    batchjobs.df_poitems_out['metric_pr_age_since_create'] = batchjobs.df_poitems_out.apply(
        lambda x: dutils.days_diff(x['zmm_pr_creation_date'],x['pr_aging_end_date']),
        axis=1
    )
    batchjobs.df_poitems_out['metric_pr_age_since_H00'] = batchjobs.df_poitems_out.apply(
        lambda x: None if sap_helpers.is_blank(x['time_H00_completed']) else dutils.days_diff(x['time_H00_completed'],x['pr_aging_end_date']),
        axis=1
    )


def po_aging(batchjobs):
    """ Add the calculated field of PO age """
    dt_today = datetime.datetime.now().date()

    batchjobs.df_poitems_out['time_po_aging_start_date'] = batchjobs.df_poitems_out.apply(
        lambda x: x['me2m_document_date'] if sap_helpers.is_blank(x['zmm_po_approved_date_max']) else x['zmm_po_approved_date_max'],
        axis = 1
    )

    batchjobs.df_poitems_out['time_po_aging_end_date'] = batchjobs.df_poitems_out.apply(
        lambda x: dt_today if sap_helpers.is_blank(x['zmm_gr_date_min']) else x['zmm_gr_date_min'],
        axis = 1
    )
    batchjobs.df_poitems_out['metric_po_age'] = batchjobs.df_poitems_out.apply(
        lambda x: dutils.days_diff(x['time_po_aging_start_date'],x['time_po_aging_end_date']),
        axis = 1
    )


def date_dimensions(batchjobs):
    """ Add calculated date dimension fields """
    dt_today = datetime.datetime.now().date()
    batchjobs.df_poitems_out['dim_time_year_pr_created'] = batchjobs.df_poitems_out.zmm_pr_creation_date.dt.year.fillna(0).astype(int).astype(str)
    batchjobs.df_poitems_out['dim_time_quarter_pr_created'] = batchjobs.df_poitems_out['dim_time_year_pr_created'] + '-Q' + batchjobs.df_poitems_out.zmm_pr_creation_date.dt.quarter.fillna(0).astype(int).astype(str)
    batchjobs.df_poitems_out['dim_time_yearmonth_pr_created'] = batchjobs.df_poitems_out['zmm_pr_creation_date'].apply(lambda x: x.strftime("%Y-%m") if not pd.isnull(x) else None)

    batchjobs.df_poitems_out['dim_time_year_po_created'] = batchjobs.df_poitems_out.me2m_document_date.dt.year.fillna(0).astype(int).astype(str)
    batchjobs.df_poitems_out['dim_time_quarter_po_created'] = batchjobs.df_poitems_out['dim_time_year_po_created']  + '-Q' +  batchjobs.df_poitems_out.me2m_document_date.dt.quarter.fillna(0).astype(int).astype(str)
    batchjobs.df_poitems_out['dim_time_yearmonth_po_created'] = batchjobs.df_poitems_out['me2m_document_date'].apply(lambda x: x.strftime("%Y-%m") if not pd.isnull(x) else None)

def foreign_local(batchjobs):
    """ Add calculated date dimension fields """
    def xfm_foreign_local(currency):
        return('LOCAL' if currency == 'PHP' else 'FOREIGN')

    #batchjobs.df_poitems_out['dim_local_foreign'] = batchjobs.df_poitems_out['zmm_currency'].apply(xfm_foreign_local)
    batchjobs.df_poitems_out['dim_local_foreign'] = batchjobs.df_poitems_out['zmm_currency2'].apply(xfm_foreign_local)

def emergency_longform_routine(batchjobs):
    """ Add calculated Emergency/ Long-form/ Routine dimension field for use by Site Fulfillment """
    def xfm_emergency_longform_routine(df_row):
        if (str(df_row['dim_pr_series_no']) == '5'):
            return('Emergency')
        elif (df_row['me2m_purchasing_doc_type'] == 'CPO'):
            return('Long-form')
        else:
            return('Others')
    
    batchjobs.df_poitems_out['dim_emergency_longform_routine'] = batchjobs.df_poitems_out.apply(xfm_emergency_longform_routine, axis=1)

def sla_days_PR2PO(batchjobs):
    """ Add calculated with SLA for PR to PO days based on Emergency/ Long-form/ Routine type """
    def xfm_sla_days_PR2PO(pr_sla_group):
        if(pr_sla_group == 'Long-form'):
            return(90)
        elif(pr_sla_group == 'Emergency'):
            return(15)
        else:
            return(35)

    batchjobs.df_poitems_out['sla_days_PR2PO'] = batchjobs.df_poitems_out['dim_emergency_longform_routine'].apply(xfm_sla_days_PR2PO)

def pr_po_age_bins(batchjobs):
    """ Binning of PR age and PO age for Outstanding PR-items and Open PO-items """
    def age_bins(n_days):
        if (n_days ==  None):
            return(None)
        elif (n_days > 90):
            return('e. Over 90 days')
        elif (n_days > 60):
            return('d. 61 - 90 days')
        elif (n_days > 30):
            return('c. 31 - 60 days')
        elif (n_days > 15):
            return('b. 15 - 30 days')
        else:
            return('a. <= 15 days')
    #dim_age_bin_pr_outstanding
    batchjobs.df_poitems_out['dim_bins_age_pr_outstanding'] = batchjobs.df_poitems_out.apply(
        lambda x: age_bins(x['metric_pr_age_since_create']) if x['status_yesno_pr_outstanding'] else None,
        axis=1
    )
    #dim_age_bin_po_open
    batchjobs.df_poitems_out['dim_bins_age_po_open'] = batchjobs.df_poitems_out.apply(
        lambda x: age_bins(x['metric_po_age']) if x['status_yesno_po_open'] else None,
        axis=1
    )

def spend_amount_bins(batchjobs):
    """ Binning by spend amount """
    def spend_bins(n):
        try:
            M = n/1000000
        except Exception as err:
            M = None

        if (M ==  None):
            return(None)
        elif (M > 500):
            return('e. > 500M')
        elif (M > 100):
            return('d. 100 - 500M')
        elif (M > 15):
            return('c. 15 - 100M')
        elif (M > 2):
            return('b. 2 - 15M')
        elif (M > 0):
            return('a. 0 - 2M')
        elif (M <= 0):
            return('0. <= 0')
        else:
            return('a. <= 15 days')

    batchjobs.df_poitems_out['dim_bins_po_order_value'] = batchjobs.df_poitems_out['me2m_net_order_value'].apply(spend_bins)
    batchjobs.df_poitems_out['dim_bins_pr_order_value'] = batchjobs.df_poitems_out['zmm_valuation_price'].apply(spend_bins)

    #batchjobs.df_poitems_out['dim_bins_po_order_value'] = pd.qcut(batchjobs.df_poitems_out['me2m_net_order_value'], 5).astype(str)
    #batchjobs.df_poitems_out['dim_bins_pr_order_value'] = pd.qcut(batchjobs.df_poitems_out['zmm_valuation_price'], 5).astype(str)

def rename_columns(batchjobs):
    """ Rename & resort dataframe columns and choose subset to retain """
    def xfm_rename_cols(df):
        # Add 'n'column with value of 1 for convenient counting
        df['metric_n'] = 1
        
        # Apply renaming map
        df.rename(columns=batchjobs.lookup_renaming_map, inplace=True)
        
        # Select all of the dimensions & metrics
        cols_id = df.filter(regex='^id_').columns.sort_values()
        cols_dim = df.filter(regex='^dim_').columns.sort_values()
        cols_time = df.filter(regex='^time_').columns.sort_values()
        cols_met = df.filter(regex='^metric_').columns.sort_values()
        cols_num = df.filter(regex='^num_').columns.sort_values()
        cols_detail = df.filter(regex='^detail_').columns.sort_values()
        cols_sla = df.filter(regex='^sla_').columns.sort_values()
        cols_status = df.filter(regex='^status_').columns.sort_values()
        cols_subcharts = df.filter(regex='^subchart_').columns.sort_values()

        cols_include = cols_id | cols_time | cols_dim | cols_sla | cols_status | cols_met | cols_num | cols_detail | cols_subcharts

        #cols_others = list(set(df.columns) - set(cols_include))

        # Drop empty  rows & columns
        #df.dropna(axis='columns', how='all', inplace=True)
        #df.dropna(axis='index', how='all', inplace=True)

        # Exclude columns?
        #cols_exclude = ['pr_aging_end_date', 'po_aging_end_date']
        #cols_exclude = ['datetime_loaded','date_loaded']
        #final_cols = list(set(cols_all).difference(cols_exclude))
        #final_cols.sort()
        
        df = df[cols_include]       
        return(df)

    batchjobs.df_poitems_out = xfm_rename_cols(batchjobs.df_poitems_out)

def po_due_target(batchjobs):
    """ Target PO release due date
    
        Revised PO Release Date	Revised target dates (depends if goods/services and local/foreign)
        =IF(AND(CT2="GOODS-LOCAL"),(CH2-69+52),
        IF(AND(CT2="GOODS-FOREIGN"),(CH2-244+135),
        IF(AND(CT2="SERVICES-LOCAL"),(CH2-90+52),
        (CH2-135))))

    """
    def xfm_po_due_target(df_row):
        """ Determine if an item is goods or services based on the material group """
        if ((df_row['dim_goods_services'].lower() == 'goods') & (df_row['dim_local_foreign'].lower() == 'local')):
            # Local goods
            return(df_row['zmm_pr_creation_date'] + timedelta(days=52))
        elif ((df_row['dim_goods_services'].lower() == 'goods') & (df_row['dim_local_foreign'].lower() == 'foreign')):
            # Foreign goods
            return(df_row['zmm_pr_creation_date'] + timedelta(days=135))
        elif ((df_row['dim_goods_services'].lower() in ['services','new services']) & (df_row['dim_local_foreign'].lower() == 'foreign')):
            # Foreign services
            return(df_row['zmm_pr_creation_date'] + timedelta(days=109))
        else:
            # Local services
            return(df_row['zmm_pr_creation_date'] + timedelta(days=52))

    batchjobs.df_poitems_out['sla_po_release_target_date'] = batchjobs.df_poitems_out.apply(xfm_po_due_target, axis=1)

def delivery_due_target(batchjobs):
    """ Target delivery due date
    
        Revised Delivery Date	Revised target dates (depends if goods/services and local/foreign) =
        IF(AND(CT2="GOODS-LOCAL"),(L2+69),
        IF(AND(CT2="GOODS-FOREIGN"),(L2+244),
        IF(AND(CT2="SERVICES-LOCAL"),(L2+90),
        (L2+244))))

=IF(AND(CT1286="GOODS-LOCAL"),(L1286+69),
IF(AND(CT1286="GOODS-FOREIGN"),(L1286+244),
IF(AND(CT1286="SERVICES-LOCAL"),(L1286+90),
(L1286+244)
)))

        Revised PO Release Date	Revised target dates (depends if goods/services and local/foreign) = IF(AND(CT2="GOODS-LOCAL"),(CH2-69+52),IF(AND(CT2="GOODS-FOREIGN"),(CH2-244+135),IF(AND(CT2="SERVICES-LOCAL"),(CH2-90+52),(CH2-135))))

    """
    def xfm_delivery_due_target(df_row):
        """ Determine if an item is goods or services based on the material group """
        if ((df_row['dim_goods_services'].lower() == 'goods') & (df_row['dim_local_foreign'].lower() == 'local')):
            # Local goods
            return(df_row['zmm_pr_creation_date'] + timedelta(days=69))
        elif ((df_row['dim_goods_services'].lower() == 'goods') & (df_row['dim_local_foreign'].lower() == 'foreign')):
            # Foreign goods
            return(df_row['zmm_pr_creation_date'] + timedelta(days=244))
        elif ((df_row['dim_goods_services'].lower() in ['services','new services']) & (df_row['dim_local_foreign'].lower() == 'foreign')):
            # Foreign services
            return(df_row['zmm_pr_creation_date'] + timedelta(days=244))
        else:
            # Local services
            return(df_row['zmm_pr_creation_date'] + timedelta(days=90))

    batchjobs.df_poitems_out['sla_delivery_target_date'] = batchjobs.df_poitems_out.apply(xfm_delivery_due_target, axis=1)

def actual_vs_po_release_target(batchjobs):
    """ Days from today until PO Release target date
    
    """
    dt_today = datetime.datetime.now().date()
    batchjobs.df_poitems_out['metric_days_past_po_release_target'] = batchjobs.df_poitems_out.apply(
        lambda x: dutils.days_diff(x['sla_po_release_target_date'], dt_today), 
        axis=1
    )
    """ Additional notes:
    OUTSTANDING (STATUS) v2

    CO: =IF(C159="Outstanding",BC159-CI159,"")
    BC: Current Date
    CI: Revised PO Release Date


    =IF(CO173="","",
    IF(CO173<-15,"Due in Future Sprints",
    IF(AND(CO173>-15,CO173<1),"Due within 2 weeks",
    IF(AND(CO173>0,CO173<31),"Below 30 Days",
    IF(AND(CO173>30,CO173<60),"31 to 60 Days",
    IF(AND(CO173>60,CO173<91),"61 to 90 Days",
    "Over 90 Days"))))))

    """

    def pastdue_bins(n_days):
        if (n_days ==  None):
            return(None)
        elif (n_days > 90):
            return('f. >90 days past due')
        elif (n_days > 60):
            return('e. 61 - 90 days past due')
        elif (n_days > 30):
            return('d. 31 - 60 days past due')
        elif (n_days >= 1):
            return('c. 1 - 30 days past due')
        elif ((n_days >= -15) & (n_days <= 0)):
            return('b. Due within 2 weeks')
        elif (n_days < -15):
            return('a. Due in the future')
        else:
            return(None)
    #dim_days_past_po_release_target_bin
    batchjobs.df_poitems_out['dim_bins_days_past_po_release_target'] = batchjobs.df_poitems_out.apply(
        lambda x: pastdue_bins(x['metric_days_past_po_release_target']) if (x['status_yesno_po_processed']==False) else None,
        axis=1
    )

def actual_vs_delivery_target(batchjobs):
    """ Days from today until delivery target date
    
    """
    dt_today = datetime.datetime.now().date()
    batchjobs.df_poitems_out['metric_days_past_delivery_target'] = batchjobs.df_poitems_out.apply(
        lambda x: dutils.days_diff(x['sla_delivery_target_date'], dt_today),
        axis=1
    )
    """ Additional notes:
        =IF(CO2="","",IF(CO2<-15,"Due in Future Sprints",
        IF(AND(CO2>-15,CO2<1),"Due within 2 weeks",
        IF(AND(CO2>0,CO2<31),"Below 30 Days",
        IF(AND(CO2>30,CO2<60),"31 to 60 Days",
        IF(AND(CO2>60,CO2<91),"61 to 90 Days",
        "Over 90 Days"))))))
    """
    def pastdue_bins(n_days):
        if (n_days ==  None):
            return(None)
        elif (n_days > 90):
            return('f. >90 days past due')
        elif (n_days > 60):
            return('e. 61 - 90 days past due')
        elif (n_days > 30):
            return('d. 31 - 60 days past due')
        elif (n_days >= 1):
            return('c. 1 - 30 days past due')
        elif ((n_days >= -15) & (n_days <= 0)):
            return('b. Due within 2 weeks')
        elif (n_days < -15):
            return('a. Due in the future')
        else:
            return(None)

    #batchjobs.df_poitems_out['dim_days_until_delivery_target_bin'] = batchjobs.df_poitems_out.apply(
    #    lambda x: due_bins(x['metric_days_until_delivery_target']) if x['status_yesno_po_open'] else None,
    #    axis=1
    #)
    #dim_days_past_delivery_target_bin
    batchjobs.df_poitems_out['dim_bins_days_past_delivery_target'] = batchjobs.df_poitems_out.apply(
        lambda x: pastdue_bins(x['metric_days_past_delivery_target']) if x['status_yesno_po_open'] else None,
        axis=1
    )

def incoterms(batchjobs):
    """ Incoterms fields
    
    """

    # Rename columns
    #batchjobs.df_poitems_out['dim_incoterms_code'] = batchjobs.df_poitems_out['expd_incoterms']
    #batchjobs.df_poitems_out['dim_incoterms_details'] = batchjobs.df_poitems_out['expd_incoterms_part_2']
    batchjobs.df_poitems_out.rename(columns={
        'expd_incoterms':'dim_incoterms_code',
        'expd_incoterms_part_2':'dim_incoterms_details'
    }, inplace=True)
    
    # New column with code + details
    batchjobs.df_poitems_out['dim_incoterms'] = batchjobs.df_poitems_out.apply(
        lambda x:  None if sap_helpers.is_blank(x['dim_incoterms_code']) else str(x['dim_incoterms_code']) + ' - ' + str(x['dim_incoterms_details']),
        axis=1
    )

def io_number(batchjobs):
    """ IO number
        Because ZMMPRSTAT sometimes has a mistake, first use ME2M to get the IO number if it is available there.
    """
    def clean_order_no(x):
        #if(isinstance(x,NoneType))
        if (sap_helpers.is_blank(x)):
            return(None)
        else:
            try:
                i = int(x)
                s = str(i)
            except:
                s = x
            return(s)
        
    batchjobs.df_poitems_out['dim_io_number'] = batchjobs.df_poitems_out.apply(
        lambda x: clean_order_no(x['zmm_order']) if sap_helpers.is_blank(x['acct_order']) else clean_order_no(x['acct_order']),
        axis = 1
    )

def io_mappings(batchjobs):
    # Make sure lookup tables are available
    if( (batchjobs.lookup_io_l2 == None) | (batchjobs.lookup_io_description == None) ):
        batchjobs.load_lookup_tables()

    # Apply 'VLookups' to the following dimensions
    batchjobs.df_poitems_out['dim_l2_department'] = batchjobs.df_poitems_out['dim_io_number'].map(batchjobs.lookup_io_l2)
    batchjobs.df_poitems_out['dim_io_description'] = batchjobs.df_poitems_out['dim_io_number'].map(batchjobs.lookup_io_description)
    
    
def join_kissflow(batchjobs):
    """ Related Kissflow fields  """
    # TODO - utilize the full list of PR numbers in the Kissflow records
    def extract_nums(s):
        """ Gives a list of numbers from within a string """
        [int(s) for s in str.split() if s.isdigit()]

    # Ensure data is available
    if ( (not isinstance(batchjobs.df_kissflow_awards,pd.DataFrame)) | (not isinstance(batchjobs.df_kissflow_prvs,pd.DataFrame)) ):
        log.info("Kissflow data hasn't been loaded. Attempting to load.")
        batchjobs.get_kissflow_data()
    
    if (batchjobs.df_kissflow_awards.empty):
        log.warning("Kissflow awards data is empty.")

    if (batchjobs.df_kissflow_prvs.empty):
        log.warning("Kissflow PRV data is empty.")
    
    # Get a copy of the data (and do not modify the original)
    df_awards = batchjobs.df_kissflow_awards
    df_prvs = batchjobs.df_kissflow_prvs

    # Trim PR to first 10 digits (if multiple PRs, just take the first one)
    df_awards['kawards_pr_number'] = df_awards['kawards_pr_number'].apply(lambda x: x[0:10])
    df_prvs['kprv_pr_number'] = df_prvs['kprv_pr_number'].apply(lambda x: x[0:10])

    # Group by PR, and make ids & awards basis into lists (to avoid duplicate records in downstream calculations)
    df_awards = df_awards.groupby('kawards_pr_number').agg(
        {'dim_kissflow_awards_basis' : lambda x: ', '.join(x),
        'id_kissflow_awards' : lambda x: ', '.join(x)})

    df_prvs = df_prvs.groupby('kprv_pr_number').agg(
        {'dim_kissflow_pr_type' : lambda x: ', '.join(x),
        'id_kissflow_prv' : lambda x: ', '.join(x)})
    # Add the calculated fields to SAP data table via merge (join)
    df_sap = batchjobs.df_poitems_out
    df_merge1 = df_sap.merge(df_prvs, how='left', left_on='id_pr_no', right_on='kprv_pr_number')
    batchjobs.df_poitems_out = df_merge1.merge(df_awards, how='left', left_on='id_pr_no', right_on='kawards_pr_number')